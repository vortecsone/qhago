package com.example.tracermodule.view

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.tracermodule.R
import kotlinx.android.synthetic.main.activity_tracer.*
import me.impa.pinger.PingInfo
import me.impa.pinger.Pinger

/*
TracerActivity class is used to  displaying possible routes and
measuring transit delays of packets across an Internet Protocol network.
*/
class TracerActivity : AppCompatActivity() {
    private var pinger: Pinger? = null;

    private var fromIp: String? = null
    private var toIp: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracer)


        /* val bundle = intent.extras
         bundle?.let {

             bundle.apply {
                 //Intent with data
                 fromIp = bundle!!.getString("from_ipAddress")!!
                 toIp = bundle!!.getString("to_ipAddress")!!

                 hostname.setText(fromIp)

             }
         }
 */


        pingbutton.setOnClickListener { onButtonClick() }
        pinglog.setMovementMethod(ScrollingMovementMethod())


    }

    fun addToLog(line: String) {
        runOnUiThread {
            pinglog.text = "${pinglog.text}\n$line";
            val scrollAmount = pinglog.layout.getLineTop(pinglog.lineCount) - pinglog.height
            pinglog.scrollTo(0, 0.coerceAtLeast(scrollAmount))
        }
    }

    fun Stop() {
        pinger?.StopAll()
        pinger = null
        runOnUiThread {
            pingbutton.text = "Start"
        }
    }


    fun onButtonClick() {

        if (checkForInternet(this)) {

            if (pinger != null) {
                Stop()
                return
            }

            pinger = Pinger();
            pinger?.setOnPingListener(object : Pinger.OnPingListener {
                override fun OnTimeout(pingInfo: PingInfo, sequence: Int) {
                    addToLog("#$sequence: Timeout!")
                    if (sequence >= 10)
                        Stop()
                }

                override fun OnReplyReceived(pingInfo: PingInfo, sequence: Int, timeMs: Int) {

                    addToLog("#$sequence: Reply from ${pingInfo.RemoteIp}: bytes=${pingInfo.Size} time=$timeMs TTL=${pingInfo.Ttl}")
                    if (sequence >= 10)
                        Stop()
                }

                override fun OnSendError(pingInfo: PingInfo, sequence: Int) {
                    addToLog("#$sequence: PING error!")
                }

                override fun OnStop(pingInfo: PingInfo) {
                    addToLog("Trace complete!")
                }

                override fun OnStart(pingInfo: PingInfo) {
                    addToLog("Pinging ${pingInfo.ReverseDns} [${pingInfo.RemoteIp}] with ${pingInfo.Size} bytes of data:")
                }

                override fun OnException(pingInfo: PingInfo, e: Exception, isFatal: Boolean) {
                    addToLog("$e")
                    if (isFatal)
                        Stop()
                }


            })
            pinger?.Ping(hostname.text.toString())
            pingbutton.text = "Stop"

        } else {
            Toast.makeText(this, "Please enable your internet connection..", Toast.LENGTH_SHORT)
                .show()

        }


    }
}