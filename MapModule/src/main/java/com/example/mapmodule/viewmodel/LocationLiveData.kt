package com.mayowa.android.locationwithlivedata

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import androidx.lifecycle.LiveData
import com.example.mapmodule.other.Constants
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import kotlin.math.*

/*
    *  LocationLiveData is user to implementation of Android Location API...
 */

 class LocationLiveData(context: Context) : LiveData<LocationModel>() {

    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
     @SuppressLint("MissingPermission")
    override fun onInactive() {
        super.onInactive()
        fusedLocationClient.removeLocationUpdates(locationCallback)

    }

/*
    *  Get Updated location when App is in Active mode...
*/

    @SuppressLint("MissingPermission")
    override fun onActive() {
        super.onActive()
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.also {
                    setLocationData(it)
                }
            }
        startLocationUpdates()
    }


/*
   * Start for location Update ...

*/

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    /*
        *  For return list of location and set Latitude and Longitude...
    */

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            for (location in locationResult.locations) {
                setLocationData(location)
            }


        }

    }

    /*
       *   For set location value ...
    */

    private fun setLocationData(location: Location) {
        value = LocationModel(
            longitude = location.longitude,
            latitude = location.latitude
        )
    }


    /*
       *  For Time interval for Location request in Map...
    */
    companion object {
        val locationRequest: LocationRequest = LocationRequest.create().apply {
            interval = Constants.LOCATION_INTERVAL
            fastestInterval = Constants.FASTEST_LOCATION_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }
}

/*
    * Created model class for get Latitude and Longitude value...
*/

data class LocationModel(
    val longitude: Double,
    val latitude: Double
)
