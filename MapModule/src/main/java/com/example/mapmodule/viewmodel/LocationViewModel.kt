package com.mayowa.android.locationwithlivedata

import android.app.Application
import androidx.lifecycle.AndroidViewModel

/*
  *  LocationViewModel is used to fetch location data and hold the latitude and longitude.
*/

class LocationViewModel(application: Application) : AndroidViewModel(application) {

    private val locationData = LocationLiveData(application)
    fun getLocationData() = locationData
}
