package com.example.mapmodule.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.app.PictureInPictureParams
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Point
import android.os.*
import android.util.Log
import android.util.Rational
import android.view.Gravity
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModelProviders
import androidx.savedstate.SavedStateRegistry
import com.example.mapmodule.R
import com.example.mapmodule.other.Constants
import com.example.mapmodule.utils.GpsUtils
import com.example.mapmodule.utils.MapUtil
import com.example.mapmodule.utils.checkForInternet
import com.google.android.gms.common.wrappers.Wrappers.packageManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import com.mayowa.android.locationwithlivedata.LocationViewModel
import android.content.pm.PackageManager as PackageManager


/*
MapsActivity class is used to
* Manipulates the map once available.
* This callback is triggered when the map is ready to be used.
*/

@Suppress("DEPRECATION")
class MapsActivity : FragmentActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var toPosition: LatLng? = null
    private lateinit var locationViewModel: LocationViewModel
    private var isGPSEnabled = false
    var locationArrayList = ArrayList<LatLng>()
    lateinit var polylineOptions: PolylineOptions
    private var isFirstLatlong: LatLng? = null
    private lateinit var marker: Marker
    @RequiresApi(Build.VERSION_CODES.M)
    lateinit var latlng: LatLng


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        /*
        *Obtain the SupportMapFragment and get notified when the map is ready to be used.
         */

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        polylineOptions = PolylineOptions()
        polylineOptions.color(Constants.POLYLINE_COLOR)
        polylineOptions.width(Constants.POLYLINE_WIDTH)
        locationArrayList = ArrayList()
//    Initilized LocationViewModel class..

        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {

            override fun gpsStatus(isGPSEnable: Boolean) {
                this@MapsActivity.isGPSEnabled = isGPSEnable
            }
        })

        /*
         check internet connection..
        */

        invokeLocationAction()
        checkForInternetConnection(this)


    }


    override fun onStart() {
        super.onStart()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

    }

    override
    fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        invokeLocationAction()
        checkForInternetConnection(this)

        /*
              prevent crashing if the map doesn't exist yet (eg. on starting activity)
        */

        if (mMap != null) {
            mMap!!.clear();
        }
    }

    override fun onPause() {
        super.onPause()


        /*  val pm: PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

          val screenOn: Boolean
          screenOn = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
              pm.isInteractive()
          } else {
              pm.isScreenOn()
          }

          if (screenOn) {
              // Screen is still on, so do your thing here


          }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GPS_REQUEST) {
                isGPSEnabled = true
                invokeLocationAction()
            }
        }
    }

    /*
        * for checked the GPS permission...
    */

    private fun invokeLocationAction() {
        when {
            !isGPSEnabled -> Toast.makeText(this, "Please enable your gps", Toast.LENGTH_SHORT)
                .show()
            isPermissionsGranted() -> startLocationUpdate()

            shouldShowRequestPermissionRationale() -> Toast.makeText(
                this,
                "App requires location permission",
                Toast.LENGTH_SHORT
            ).show()

            else -> ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                LOCATION_REQUEST
            )
        }
    }


    /*
       *  for get current loaction latitude and longitude value ...
    */
    private fun startLocationUpdate() {

        /*
           * create a list for updated location ..
        */
        //   locationArrayList = ArrayList()


        /*
        * create a polyline
        */
        /*  val polylineOptions = PolylineOptions()
          polylineOptions.color(Constants.POLYLINE_COLOR)
          polylineOptions.width(Constants.POLYLINE_WIDTH)*/
        locationViewModel.getLocationData().observe(this, Observer {
            if (isFirstLatlong == null) {
                isFirstLatlong = LatLng(it.latitude, it.longitude)

                // Clears the previous  position
                mMap!!.clear();
                // below line is use to add marker to each location of our array list.
                marker = mMap!!.addMarker(MarkerOptions().position(isFirstLatlong).title("User"))
                mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID

                mMap!!.uiSettings.isZoomControlsEnabled = true
                //  mMap!!.animateCamera(CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM))
                val location = CameraUpdateFactory.newLatLngZoom(isFirstLatlong, 19F)
                mMap!!.animateCamera(location)
                // below line is use to move our camera to the specific location.
                //   mMap!!.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList[i]))

            }
            latlng = LatLng(it.latitude, it.longitude)

            val toast = Toast.makeText(
                this,
                "Updated Location : " + it.latitude + " , " + it.longitude,
                Toast.LENGTH_SHORT
            )

            toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.show()

//            Distance between two user location..

            val distance = SphericalUtil.computeDistanceBetween(isFirstLatlong, latlng)
            Toast.makeText(
                this, "Distance between Source and Destination  is \n " + String.format(
                    "%.2f",
                    distance!! / 1000
                ) + "km", Toast.LENGTH_SHORT
            ).show();


            /*
               * Added latitude and Longitude value in list..
           */
//            var  latLng=LatLng(it.latitude, it.longitude)
            locationArrayList.add(latlng)
            if (distance > 20) {
                locationArrayList.remove(isFirstLatlong!!)

                for (i in locationArrayList.indices) {
                    // Clears the previous  position
                    mMap!!.clear();
                    // below line is use to add marker to each location of our array list.
                    mMap!!.addMarker(MarkerOptions().position(locationArrayList[i]).title("User"))
                    mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                    isFirstLatlong = latlng
                    mMap!!.uiSettings.isZoomControlsEnabled = true
                    val location = CameraUpdateFactory.newLatLngZoom(locationArrayList[i], 19F)
                    mMap!!.animateCamera(location)
                    //  mMap!!.animateCamera(CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM))
                    /*val location = CameraUpdateFactory.newLatLngZoom(latlng, 19F)
                    mMap!!.animateCamera(location)*/
                    // below line is use to move our camera to the specific location.
                    //   mMap!!.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList[i]))
                    polylineOptions.add(latlng)
                    mMap!!.addPolyline(polylineOptions)
                }
            } else {
                // below line is use to add marker to each location of our array list.
                marker.position = (latlng)
                marker.title = "User"
                val location = CameraUpdateFactory.newLatLngZoom(latlng, 19F)
                mMap!!.animateCamera(location)
            }

            /*
               *  used to zoom our camera on map...
            */
//            mMap!!.uiSettings.isZoomControlsEnabled=true
//            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(Constants.MAP_ZOOM))

            /*
            * Added  multiple coordinate Polyline in Map
            */

//            for (markerCoordinate in locationArrayList) {
//                polylineOptions.add(markerCoordinate)
//            }


            /*
               * Added  coordinate Polyline in Map
          */

//            mMap!!.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }

        )
    }

    override fun onDestroy() {
        super.onDestroy()
        /*
        * Clear the location list when App destroy
        */
        locationArrayList.clear()
    }


    /*
       * For checked Internet permission...
    */

    private fun checkForInternetConnection(mapsActivity: MapsActivity) {

        if (checkForInternet(mapsActivity)) {

//            Reterived location data from Mesibo location

            /*val bundle: Bundle? = intent.extras
            bundle?.let {

                bundle.apply {
                    //Intent with data
                    val bundle = intent.getParcelableExtra<Bundle>("bundle")
//                    fromPosition = bundle?.getParcelable("from_position")
                    toPosition = bundle?.getParcelable("to_position")


                }
            }*/

        } else {
            Toast.makeText(this, "Internet is not Connected", Toast.LENGTH_SHORT).show()

        }

    }

    private fun isPermissionsGranted() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED

    private fun shouldShowRequestPermissionRationale() =
        ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) && ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST -> {
                invokeLocationAction()
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.getUiSettings().setZoomControlsEnabled(true)
        mMap!!.getUiSettings().setZoomGesturesEnabled(true)
        mMap!!.getUiSettings().setCompassEnabled(true)
        setMapStyle(mMap!!)
    }

    private fun setMapStyle(mMap: GoogleMap) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.map_style
                )
            )
            if (!success) {
                Log.e(MapsActivity::class.java.simpleName, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(MapsActivity::class.java.simpleName, "Can't find style. Error: ", e)
        }

    }

    /*
       * For Picture in  Picture mode in Map...
    */

    override fun onUserLeaveHint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val pipParam = PictureInPictureParams.Builder()
            val display = windowManager.defaultDisplay
            val point = Point()
            display.getSize(point)
            pipParam.setAspectRatio(Rational(point.x, point.y))
            enterPictureInPictureMode(pipParam.build())
        }

    }

    override fun onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val pipParam = PictureInPictureParams.Builder()
            val display = windowManager.defaultDisplay
            val point = Point()
            display.getSize(point)
            pipParam.setAspectRatio(Rational(point.x, point.y))
            enterPictureInPictureMode(pipParam.build())

        }
    }


}

const val LOCATION_REQUEST = 100
const val GPS_REQUEST = 101


