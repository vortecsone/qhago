package com.example.mapmodule.utils

import android.content.Context
import android.graphics.*
import com.example.mapmodule.R
import com.google.android.gms.maps.model.LatLng
import kotlin.math.abs
import kotlin.math.atan

/*  MapUtil is used to create marker option
       and for marker animation..
   */
object MapUtil {



    fun getRotation(start: LatLng, end: LatLng): Float {
        val latDifference: Double = abs(start.latitude - end.latitude)
        val lngDifference: Double = abs(start.longitude - end.longitude)
        var rotation = -1F
        when {
            start.latitude < end.latitude && start.longitude < end.longitude -> {
                rotation = Math.toDegrees(atan(lngDifference / latDifference)).toFloat()
            }
            start.latitude >= end.latitude && start.longitude < end.longitude -> {
                rotation = (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 90).toFloat()
            }
            start.latitude >= end.latitude && start.longitude >= end.longitude -> {
                rotation = (Math.toDegrees(atan(lngDifference / latDifference)) + 180).toFloat()
            }
            start.latitude < end.latitude && start.longitude >= end.longitude -> {
                rotation =
                    (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 270).toFloat()
            }
        }
        return rotation
    }

    /**
     * This function returns the list of locations of Car during the trip i.e. from Origin to Destination
     */



    /**
     * This function returns the list of locations of user during the trip i.e. from previous location  to current location
     */

//    fun getListOfUpdatedLocations(latLng: LatLng): ArrayList<LatLng> {
//
//        val locationList = ArrayList<LatLng>()
//        val usrLocationlIST=locationList.distinct()
//       if (usrLocationlIST.size>0){
//           locationList.add(LatLng(latLng.latitude, latLng.longitude))
//       }
//
//        return locationList
//    }

}