package com.example.mapmodule.other

import android.graphics.Color

object Constants {
    const val LOCATION_UPDATE_INTERVAL = 5000L
    const val FASTEST_LOCATION_INTERVAL = 3000L
    const val LOCATION_INTERVAL = 2000L
    const val POLYLINE_COLOR = Color.RED
    const val POLYLINE_WIDTH = 5f
    const val MAP_ZOOM = 22.0f
}