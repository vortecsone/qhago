package com.example.stripemodule

import android.net.TrafficStats
import android.os.Bundle
import android.os.StrictMode
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stripemodule.databinding.StoreActivityBinding
import com.example.stripemodule.service.SampleStoreEphemeralKeyProvider
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.stripe.android.CustomerSession
import com.stripe.android.GooglePayJsonFactory
import com.stripe.android.PaymentConfiguration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StoreActivity : AppCompatActivity()
{
    private val viewModel: StoreViewModel by viewModels()
    var callDuration: Int? = 0

    private val viewBinding: StoreActivityBinding by lazy {
        StoreActivityBinding.inflate(layoutInflater)
    }

    private val paymentsClient: PaymentsClient by lazy {
        PaymentsClientFactory(this).create()
    }

    private val googlePayJsonFactory: GooglePayJsonFactory by lazy {
        GooglePayJsonFactory(this)
    }

    private val checkoutResultLauncher = registerForActivityResult(
        CheckoutContract()
    ) { result ->
        when (result) {
            is CheckoutContract.Result.PaymentIntent -> {
                displayPurchase(result.amount)
            }
            is CheckoutContract.Result.SetupIntent -> {
                displaySetupComplete()
            }
        }

        storeAdapter.clearItemSelections()
    }

    private val storeAdapter: StoreAdapter by lazy {
        StoreAdapter(this, checkoutResultLauncher) { hasItems ->
            if (hasItems) {
                viewBinding.fab.show()
            } else {
                viewBinding.fab.hide()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        callDuration = intent.getIntExtra("callDuration",0)

        TrafficStats.setThreadStatsTag(99999)

        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        )

        StrictMode.setVmPolicy(
            StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        )

        val settings = Settings(this)

        CoroutineScope(Dispatchers.IO).launch {
            PaymentConfiguration.init(
                this@StoreActivity,
                publishableKey = settings.publishableKey,
                stripeAccountId = settings.stripeAccountId
            )
        }

        CustomerSession.initCustomerSession(
            this,
            SampleStoreEphemeralKeyProvider(this, settings.stripeAccountId),
            shouldPrefetchEphemeralKey = false
        )





        setContentView(viewBinding.root)

        viewBinding.fab.isEnabled = false
        viewBinding.progressBar.isVisible = true
        viewModel.retrieveCustomer().observe(this) { result ->
            viewBinding.progressBar.isInvisible = true
            viewBinding.fab.isEnabled = result.isSuccess

            result.onSuccess {
                storeAdapter.customer = it
            }
        }

        isGooglePayReady()

        //viewBinding.fab.hide()
        //setSupportActionBar(findViewById(R.id.my_toolbar))

        viewBinding.storeItems.also {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(ItemDivider(this, R.drawable.item_divider))
            it.adapter = storeAdapter
        }

        viewBinding.fab.setOnClickListener {
            storeAdapter.launchPurchaseActivityWithCart(callDuration!!)
        }
        //storeAdapter.launchPurchaseActivityWithCart()

    }

    private fun displayPurchase(price: Long) {
        showSuccessDialog(
            R.string.purchase_successful,
            getString(R.string.total_price, StoreUtils.getPriceString(price, null))
        )
    }

    private fun displaySetupComplete() {
        showSuccessDialog(
            R.string.setup_successful
        )
    }

    private fun showSuccessDialog(@StringRes titleRes: Int, message: String? = null) {
        MaterialAlertDialogBuilder(this)
            .setTitle(titleRes)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    /**
     * Check that Google Pay is available and ready
     */
    private fun isGooglePayReady()
    {
        val request = IsReadyToPayRequest.fromJson(
            googlePayJsonFactory.createIsReadyToPayRequest().toString()
        )

        paymentsClient.isReadyToPay(request)
            .addOnCompleteListener { task ->
                storeAdapter.isGooglePayReady = runCatching {
                    task.isSuccessful
                }.getOrDefault(false)
            }


    }


}