CREATE DATABASE `DemoNodeJs`;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

DROP TABLE IF EXISTS `DemoNodeJs`.`Lawyer_has_Speciality` ;
DROP TABLE IF EXISTS `DemoNodeJs`.`Speciality` ;
DROP TABLE IF EXISTS `DemoNodeJs`.`Lawyer` ;
DROP TABLE IF EXISTS `DemoNodeJs`.`User` ;

-- -----------------------------------------------------
-- Schema DemoNodeJs
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `DemoNodeJs` DEFAULT CHARACTER SET utf8 ;
USE `DemoNodeJs` ;

-- -----------------------------------------------------
-- Table `DemoNodeJs`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DemoNodeJs`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `unique_id` VARCHAR(50) NOT NULL,
  `rut` VARCHAR(15) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `encrypted_password` VARCHAR(200) NOT NULL,
  `salt` VARCHAR(200) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `facial_validation` TINYINT(1) NULL,
  `facial_id` INT NULL,
  `mesibo_id` VARCHAR(100) NULL,
  `is_lawyer` TINYINT(1) NULL,

  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `unique_id_UNIQUE` (`unique_id` ASC) VISIBLE
)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `DemoNodeJs`.`UserLocation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `longitude` VARCHAR(50) NOT NULL,
  `latitude` VARCHAR(50) NOT NULL,
  INDEX `fk_UserLocation_User_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  CONSTRAINT `fk_UserLocation_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `DemoNodeJs`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;  


-- -----------------------------------------------------
-- Table `DemoNodeJs`.`Lawyer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DemoNodeJs`.`Lawyer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `title_certificate` VARCHAR(200) NULL,
  `bank` VARCHAR(50) NOT NULL,
  `account_type` VARCHAR(50) NOT NULL,
  `account_number` VARCHAR(200) NOT NULL,
  `hour_price` INT NOT NULL,
  INDEX `fk_Lawyer_User_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  CONSTRAINT `fk_Lawyer_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `DemoNodeJs`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `DemoNodeJs`.`Speciality`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DemoNodeJs`.`Speciality` (
  `id` INT NOT NULL,
  `description` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `DemoNodeJs`.`Lawyer_has_Speciality`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `DemoNodeJs`.`Lawyer_has_Speciality` (
  `lawyer_id` INT NOT NULL,
  `speciality_id` INT NOT NULL,
  PRIMARY KEY (`lawyer_id`, `speciality_id`),
  INDEX `fk_Lawyer_has_Speciality_Speciality1_idx` (`speciality_id` ASC) VISIBLE,
  INDEX `fk_Lawyer_has_Speciality_Lawyer1_idx` (`lawyer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Lawyer_has_Speciality_Lawyer1`
    FOREIGN KEY (`lawyer_id`)
    REFERENCES `DemoNodeJs`.`Lawyer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lawyer_has_Speciality_Speciality1`
    FOREIGN KEY (`speciality_id`)
    REFERENCES `DemoNodeJs`.`Speciality` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  )
ENGINE = InnoDB;


USE `DemoNodeJs` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;