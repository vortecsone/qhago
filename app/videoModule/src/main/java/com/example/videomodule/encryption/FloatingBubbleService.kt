package com.example.videomodule

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.IBinder
import android.view.*
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.bubble_widget_layout.view.*

class FloatingBubbleService : Service() {
    private lateinit var windowManager: WindowManager
    private lateinit var floatingBubble: View
    private var initialX: Int = 0
    private var initialY: Int = 0
    private var initialTouchX: Float = 0.toFloat()
    private var initialTouchY: Float = 0.toFloat()
    private lateinit var params: WindowManager.LayoutParams
    var firstName: String? = null
    var LAYOUT_FLAG = 0

    companion object {}


    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate() {
        super.onCreate()
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        loadFloatingBubble(inflater)
        val bubbleView: View = floatingBubble.bolha_layout


        bubbleView.setOnTouchListener { view, event ->
            when (event.action) {

                MotionEvent.ACTION_DOWN -> storeTouchs(event)

                MotionEvent.ACTION_MOVE -> moveBubble(event)

                MotionEvent.ACTION_UP -> openApp(event)

                else -> false

            }
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        /*   if(RegisterActivity2.activity2!=null){
               bubbleInterface=RegisterActivity2.activity2
           }*/
        return super.onStartCommand(intent, flags, startId)
    }

    private fun openApp(event: MotionEvent): Boolean {


        val diffPosicaoX = (event.rawX - initialTouchX).toInt()
        val diffPosicaoY = (event.rawY - initialTouchY).toInt()
        val singleClick: Boolean = diffPosicaoX < 5 && diffPosicaoY < 5

        if (singleClick) {
//            val intent = Intent(this@FloatingBubbleService, MainActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            startActivity(intent)
            /*val intent = Intent(this,RegisterActivity2::class.java)
            intent.action = "com.example.volleytutorialgetandpost"
            intent.putExtra(FIRST_NAME,firstName)
//            intent.putExtra(LAST_NAME,etLastName.text.toString())
//            intent.putExtra(PID,etID.text.toString())
            intent.component = ComponentName("com.example.encrptanddecryptapp", "com.example.encrptanddecryptapp.ipcReceiver.IPCBroadcastReceiver")
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            this.sendBroadcast(intent)
            startActivity(intent)*/


            val intent1 = Intent()
            intent1.action = "com.example.andy.myapplication"
            intent1.putExtra("DATAPASSED", "QSoy")
            sendBroadcast(intent1)

//            val intent = Intent()
//            intent.action = "com.example.volleytutorialgetandpost"
//            intent.putExtra(FIRST_NAME,name)
//
////            intent.component = ComponentName("com.example.encrptanddecryptapp", "com.example.encrptanddecryptapp.MainActivity")
//            intent.component = ComponentName("com.example.encrptanddecryptapp", "com.example.encrptanddecryptapp.ipcReceiver.IPCBroadcastReceiver")
////            intent.component = ComponentName("com.example.encrptanddecryptapp", "com.example.encrptanddecryptapp.MainActivity")
//
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            this.sendBroadcast(intent)
//            startActivity(intent)
            stopSelf()

        }
        return true
    }


    private fun moveBubble(event: MotionEvent): Boolean {
        params.x = initialX + (event.rawX - initialTouchX).toInt()
        params.y = initialY + (event.rawY - initialTouchY).toInt()

        windowManager.updateViewLayout(floatingBubble, params)
        return true
    }

    private fun storeTouchs(event: MotionEvent): Boolean {
        initialX = params.x
        initialY = params.y
        initialTouchX = (event.rawX)
        initialTouchY = (event.rawY)

        return true
    }


    private fun loadFloatingBubble(inflater: LayoutInflater) {
        floatingBubble = inflater.inflate(R.layout.bubble_widget_layout, null)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }


        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            LAYOUT_FLAG,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )
        params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY


        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        //Specify the view position
        params.gravity = Gravity.NO_GRAVITY
        params.x = 0
        params.y = 50

        //Add the view to the window
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.addView(floatingBubble, params)

        val closeButtonCollapsed = floatingBubble.close_btn
        closeButtonCollapsed.setOnClickListener {
            stopSelf()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (floatingBubble.isVisible) windowManager.removeView(floatingBubble)
    }
}