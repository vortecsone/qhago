package com.example.videomodule

import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.content.BroadcastReceiver
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.videomodule.encryption.textEncryptAndDecrypt.Decoder
import com.example.videomodule.encryption.textEncryptAndDecrypt.Encoder
import org.jitsi.meet.sdk.*
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL


class VideoCallActivity : AppCompatActivity() {
    private val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084


     private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            onBroadcastReceived(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_call)
   val serverURL: URL
        serverURL = try {
            // When using JaaS, replace "https://meet.jit.si" with the proper serverURL
            URL("https://meet.qhago.com")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }
        val defaultOptions = JitsiMeetConferenceOptions.Builder()
            .setServerURL(serverURL)
            .setFeatureFlag("startWithAudioMuted",true)
            .setFeatureFlag("startWithVideoMuted",true)
            // When using JaaS, set the obtained JWT here
            //.setToken("MyJWT")
            // Different features flags can be set
            //.setFeatureFlag("toolbox.enabled", false)
            //.setFeatureFlag("filmstrip.enabled", false)
            .setFeatureFlag("welcomepage.enabled", false)
            .build()
        JitsiMeet.setDefaultConferenceOptions(defaultOptions)

        registerForBroadcastMessages()
        startService()

    }

    override fun onStart() {
        super.onStart()
        checkOverlayPermission()
        startService()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.example.andy.myapplication")
        registerReceiver(broadcastReceiver, intentFilter)

    }

    private fun checkOverlayPermission() {
        //Check if the application has draw over other apps permission or not?
        //you have to ask for the permission in runtime.
        if (
            !Settings.canDrawOverlays(this)
        ) {

            showPermissionDialog()

        } else {

            startService()
        }

    }

    private fun showPermissionDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.apply {
            setIcon(R.drawable.ic_baseline_face_24)
            setTitle("Hello")
            setMessage("Do you want end-to-end encryption!! Please allow to display over other apps")
            setPositiveButton("Yes") { _, _ ->
                toast("clicked positive button")
                // send user to the device settings





                val intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:$packageName")

                )
                startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)

            }
            setNegativeButton("No") { _, _ ->
                toast("clicked negative button")

            }

        }.create().show()
    }
    private fun toast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }

    fun onButtonClick(v: View?) {
        val editText = findViewById<EditText>(R.id.conferenceName)




        val text = editText.text.toString()

        if (text.isNotEmpty()) {
            // Build options object for joining the conference. The SDK will merge the default
            // one we set earlier and this one when joining.
            val options = JitsiMeetConferenceOptions.Builder()
                .setRoom(text)

                // Settings for audio and video
                //.setAudioMuted(true)
                //.setVideoMuted(true)
                .build()
            // Launch the new activity with the given options. The launch() method takes care
            // of creating the required Intent and passing the options.
            JitsiMeetActivity.launch(this, options)

        }

    }


    private fun registerForBroadcastMessages() {
        val intentFilter = IntentFilter()


        /* This registers for every possible event sent from JitsiMeetSDK
           If only some of the events are needed, the for loop can be replaced
           with individual statements:
           ex:  intentFilter.addAction(BroadcastEvent.Type.AUDIO_MUTED_CHANGED.action);
                intentFilter.addAction(BroadcastEvent.Type.CONFERENCE_TERMINATED.action);
                ... other events
         */
        for (type in BroadcastEvent.Type.values()) {
            intentFilter.addAction(type.action)
            intentFilter.addAction(BroadcastEvent.Type.CHAT_TOGGLED.action)
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }



    // Example for handling different JitsiMeetSDK events
    private fun onBroadcastReceived(intent: Intent?) {
        if (intent != null) {
            var msg=""
            val event = BroadcastEvent(intent)
            when (event.type) {
                BroadcastEvent.Type.CONFERENCE_JOINED -> Timber.i(
                    "Conference Joined with url%s", event.getData().get("url"))
                BroadcastEvent.Type.PARTICIPANT_JOINED -> Timber.i(
                    "Participant joined%s", event.getData().get("name")
       )
                BroadcastEvent.Type.CHAT_TOGGLED -> Timber.i(
                    "Chat Toggele%s", event.getData().get("name")
                )
                BroadcastEvent.Type.CHAT_MESSAGE_RECEIVED -> {
                    Timber.i(
                        "Chat Toggele%s", event.getData().get("name")
                    )

                   msg =event.data["message"].toString()
                    Toast.makeText(this@VideoCallActivity, "$msg", Toast.LENGTH_SHORT).show()


                  showPassworddialog(msg)
                }
                BroadcastEvent.Type.ENDPOINT_TEXT_MESSAGE_RECEIVED -> Timber.i(
                    "Chat Toggele%s", event.getData().get("name")

                )

                else -> Timber.i("Received event: %s", event.type)
            }
        }
    }

    private fun showPassworddialog(name: String?) {

        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Enter your passcode")

// Set up the input
        val input = EditText(this)
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setHint("Enter passcode")
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)
//        val editText = findViewById<EditText>()
// Set up the buttons
        builder.setPositiveButton("OK", null)
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
            startService()
            dialog.cancel()
        })
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            // Here you get get input text from the Edittext
            var u_Password = input.text.toString()

            if (name!!.contains("==")) {
                try {

                    input.setText(
                        name.let { it1 ->
                            Decoder.decrpt(
                                this,
                                it1,
                                u_Password
                            )
                        }
                    )
                    startService()
                    dialog.dismiss()
                } catch (e: java.lang.Exception) {
                    Toast.makeText(this@VideoCallActivity, "WrongPassword", Toast.LENGTH_SHORT)
                        .show()
                    e.printStackTrace()
                }

            } else {
                if (u_Password.isEmpty()) {
                    Toast.makeText(this, "Enter passcode", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                } else {
                    input.setText(
                        name.let { it1 ->
                            Encoder.encrypt(
                                this,
                                it1,
                                u_Password
                            )
                        }
                    )
                    startService()
                    dialog.dismiss()
                }

            }
        }
    }

    // Example for sending actions to JitsiMeetSDK
    private fun hangUp() {

        val hangupBroadcastIntent: Intent = BroadcastIntentHelper.buildHangUpIntent()
        LocalBroadcastManager.getInstance(this.applicationContext)
            .sendBroadcast(hangupBroadcastIntent)
    }
    private fun startService() {
        /*val intent: Intent = Intent(
            this@RegisterActivity2,
            com.example.volleytutorialgetandpost.FloatingBubbleService(this)::class.java
        )
        intent.putExtra(FIRST_NAME,etName.text.toString())*/
        startService(Intent(this@VideoCallActivity, FloatingBubbleService::class.java))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            //Check if the permission is granted or not.
            if (resultCode == Activity.RESULT_OK) {
                startService()
            } else { //Permission is not available
                Toast.makeText(
                    this,
                    "Draw over other app permission not available. Closing the application",
                    Toast.LENGTH_SHORT
                ).show()

                finish()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)

        }
    }
}