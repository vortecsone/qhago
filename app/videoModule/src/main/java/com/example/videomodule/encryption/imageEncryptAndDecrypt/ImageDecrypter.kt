package com.example.videomodule.encryption.imageEncryptAndDecrypt

import android.content.Context
import android.graphics.Bitmap
import java.lang.Exception
import java.util.*

/*
      * Created by Chetu..
  */

object ImageDecrypter {

    var pixel: Int = 0
    var pixel2: Int = 0
    var temp: Int = 0
    var randomHeight: Int = 0
    var randomWidth: Int = 0

    @Throws(Exception::class)
    fun ImageDecrypt(
        context: Context,
        seed: Int,
        bound: Int,
        height: Int,
        width: Int,
        bitmapCopy: Bitmap?
    ): Bitmap? {

        val r = Random(seed.toLong())

        val DecryptRandomHeight = Array(width) {
            IntArray(
                height
            )


        }

        val DecryptRandomWidth = Array(width) {
            IntArray(
                height
            )
        }
        for (h in 1 until height) {
            for (w in 1 until width) {
                randomHeight = height * r.nextInt(bound) / bound
                randomWidth = width * r.nextInt(bound) / bound
                DecryptRandomHeight[w][h] = randomHeight
                DecryptRandomWidth[w][h] = randomWidth
            }
        }
        for (h in height - 1 downTo 1) {
            for (w in width - 1 downTo 1) {
                randomWidth = DecryptRandomWidth[w][h]
                randomHeight = DecryptRandomHeight[w][h]
                pixel = bitmapCopy!!.getPixel(w, h)
                pixel2 = bitmapCopy!!.getPixel(randomWidth, randomHeight)
                temp = pixel
                pixel = pixel2
                pixel2 = temp
                bitmapCopy!!.setPixel(w, h, pixel)
                bitmapCopy!!.setPixel(randomWidth, randomHeight, pixel2)
            }
        }
        return bitmapCopy
    }

}