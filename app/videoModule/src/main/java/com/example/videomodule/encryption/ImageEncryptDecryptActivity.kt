package com.example.videomodule
import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.videomodule.encryption.imageEncryptAndDecrypt.ImageDecrypter
import com.example.videomodule.encryption.imageEncryptAndDecrypt.ImageEncrypter


import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

/*
      * Created by Chetu..
  */

class ImageEncryptDecryptActivity : AppCompatActivity() {

    // permission variables
    private val TAG_RUNTIME_PERMISSION = "TAG_RUNTIME_PERMISSION"
    private val PERMISSION_REQUEST_CODE = 1

    var Info: TextView? = null
    var saveURI: TextView? = null
    var targetImage: ImageView? = null
    var ImagePath: String? = null
    var Seed: EditText? = null
    var Complexity: EditText? = null
    var URI: Uri? = null
    var textTargetURI: String? = null
    var height = 0
    var pixel: Int = 0
    var pixel2: Int = 0
    var temp: Int = 0
    var width = 0
    var bitmap: Bitmap? = null
    var bitmapCopy: Bitmap? = null
    var seed = 0
    var bound: Int = 100
    var randomHeight: Int = 0
    var randomWidth: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_encrypt_decrypt)

        val LoadImage = findViewById<Button>(R.id.ImageSelect)
        val Encrypt = findViewById<Button>(R.id.Encypt)
        val SaveImage = findViewById<Button>(R.id.Save)
        val Decrypt = findViewById<Button>(R.id.Decrypt)
        Info = findViewById<TextView>(R.id.Info)
        saveURI = findViewById<TextView>(R.id.SaveURI)
        targetImage = findViewById<ImageView>(R.id.ImageView)
        Seed = findViewById<EditText>(R.id.Seed)
        Complexity = findViewById<EditText>(R.id.Complexity)


        /*
           * if received image from other app...
        */

//        when {
//            intent?.action == Intent.ACTION_SEND -> {
//                if (intent.type?.startsWith("image/") == true) {
//                    handleSendImage(intent) // Handle single image being sent
//                }
//            }
//
//        }

        val myUri = Uri.parse(intent.extras!!.getString("imageUri"))
        if (myUri!=null){
            bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(myUri!!))
            bitmapCopy = bitmap!!.copy(Bitmap.Config.ARGB_8888, true)
            targetImage!!.setImageBitmap(bitmap)
            height = bitmap!!.getHeight()
            width = bitmap!!.getWidth()

        }

        /** Loading Image  */

        LoadImage.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, 0)
            //permission stuff
            if (!hasRuntimePermission(
                    applicationContext,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                requestRuntimePermission(
                    this@ImageEncryptDecryptActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    this.PERMISSION_REQUEST_CODE
                )
            } else {
            }
        }

        /** ENCRYPTION  */

        Encrypt.setOnClickListener(View.OnClickListener {
            if (myUri != null) {

                if (Seed!!.text.toString().isEmpty()) {
                    seed = 0
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Enter Security code to encrypt",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                } else {
                    seed = Seed!!.text.toString().toInt()
                }
                if (Complexity!!.text.toString().isEmpty()) {
                    bound = 1
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Please enter scattering coefficient!! No encryption occurred.",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                } else if (Complexity!!.text.toString().equals("0")) {
                    bound = 1
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "0 scattering is not allowed! No encryption occurred.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (Complexity!!.text.toString().length < 3) {
                    bound = 1

                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "please enter min 3 digit scattering number",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@OnClickListener
                } else {
                    bound = Complexity!!.text.toString().toInt()
                }

                val path = ImageEncrypter.ImageEncrypt(this, seed, bound, height, width, bitmapCopy)

                targetImage!!.setImageBitmap(path)

                /*
                  val r = Random(seed.toLong())
                  for (h in 1 until height) {
                      for (w in 1 until width) {
                          randomHeight = height * r.nextInt(bound) / bound
                          randomWidth = width * r.nextInt(bound) / bound
                          pixel = bitmapCopy!!.getPixel(w, h)
                          pixel2 = bitmapCopy!!.getPixel(randomWidth, randomHeight)
                          temp = pixel
                          pixel = pixel2
                          pixel2 = temp
                          bitmapCopy!!.setPixel(w, h, pixel)
                          bitmapCopy!!.setPixel(randomWidth, randomHeight, pixel2)
                      }
                  }
                  targetImage!!.setImageBitmap(bitmapCopy)
                  */

                if (bound == 1 || Complexity!!.text.toString().isEmpty()) {
                } else {
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Encrypted!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                val info =
                    "Height = $height pixels\nWidth = $width pixels\nLast Operation: Encrypt\nImage URI: $textTargetURI"
                Info!!.text = info
            } else {
                Toast.makeText(
                    this@ImageEncryptDecryptActivity,
                    "Please select an Image first!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })


        /** Saving image  */

        SaveImage.setOnClickListener {
            var outStream: FileOutputStream? = null
            if (targetImage!!.drawable != null) {
                // Write to SD Card
                try {
                    val sdCard = Environment.getExternalStorageDirectory()
                    val dir = File(sdCard.absolutePath + "/DCIM")
                    dir.mkdirs()
                    val gen = Random()
                    var n = 1000
                    n = gen.nextInt(n)
                    val photoname = "photo-$n.jpg"
                    val fileName = String.format(photoname)
                    saveURI!!.text = "Image saved in: $dir/DCIM \nImage name:$fileName"
                    val outFile = File(dir, fileName)
                    outStream = FileOutputStream(outFile)
                    bitmapCopy!!.compress(Bitmap.CompressFormat.PNG, 100, outStream)
                    outStream.flush()
                    outStream.close()
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Image saved!",
                        Toast.LENGTH_SHORT
                    ).show()
                    val info =
                        "Height = $height pixels\nWidth = $width pixels\nLast Operation: Save Image\nImage URI: $textTargetURI"
                    Info!!.text = info
                    addImageGallery(outFile)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "FileNotFoundException",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "IOException",
                        Toast.LENGTH_SHORT
                    ).show()
                } finally {
                }
            } else {
                Toast.makeText(
                    this@ImageEncryptDecryptActivity,
                    "Please select an Image first!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        /** DECRYPTION */

        Decrypt.setOnClickListener {
            if (myUri != null) {
                if (Seed!!.text.toString().isEmpty()) {
                    seed = 0
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Seed is set to 0",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    seed = Seed!!.text.toString().toInt()
                }
                if (Complexity!!.text.toString().isEmpty()) {
                    bound = 1
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Please enter scattering coefficient! No decryption occurred.",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                } else if (Complexity!!.text.toString().equals("0")) {
                    bound = 1
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "0 scattering is not allowed! No decryption occurred.",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                } else {
                    bound = Complexity!!.text.toString().toInt()
                }

                val path = ImageDecrypter.ImageDecrypt(this, seed, bound, height, width, bitmapCopy)

                targetImage!!.setImageBitmap(path)

                /*   val r = Random(seed.toLong())

                   val DecryptRandomHeight = Array(width) {
                       IntArray(
                           height
                       )
                   }
                   val DecryptRandomWidth = Array(width) {
                       IntArray(
                           height
                       )
                   }
                   for (h in 1 until height) {
                       for (w in 1 until width) {
                           randomHeight = height * r.nextInt(bound) / bound
                           randomWidth = width * r.nextInt(bound) / bound
                           DecryptRandomHeight[w][h] = randomHeight
                           DecryptRandomWidth[w][h] = randomWidth
                       }
                   }
                   for (h in height - 1 downTo 1) {
                       for (w in width - 1 downTo 1) {
                           randomWidth = DecryptRandomWidth[w][h]
                           randomHeight = DecryptRandomHeight[w][h]
                           pixel = bitmapCopy!!.getPixel(w, h)
                           pixel2 = bitmapCopy!!.getPixel(randomWidth, randomHeight)
                           temp = pixel
                           pixel = pixel2
                           pixel2 = temp
                           bitmapCopy!!.setPixel(w, h, pixel)
                           bitmapCopy!!.setPixel(randomWidth, randomHeight, pixel2)
                       }
                   }


                targetImage!!.setImageBitmap(bitmapCopy)
                */

                if (bound == 1 || Complexity!!.text.toString().isEmpty()) {
                } else {
                    Toast.makeText(
                        this@ImageEncryptDecryptActivity,
                        "Decrypted!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                val info =
                    "Height = $height pixels\nWidth = $width pixels\nLast Operation: Decrypt\nImage URI: $textTargetURI"
                Info!!.text = info
            } else {
                Toast.makeText(
                    this@ImageEncryptDecryptActivity,
                    "Please select an Image first!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    /*
       * handle and set image funtionalty

    */
    private fun handleSendImage(intent: Intent) {
        (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let {
            textTargetURI = it.toString()
            try {
                bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(it!!))
                bitmapCopy = bitmap!!.copy(Bitmap.Config.ARGB_8888, true)
                targetImage!!.setImageBitmap(bitmap)
                height = bitmap!!.getHeight()
                width = bitmap!!.getWidth()
                val info =
                    "Height = $height pixels\nWidth = $width pixels\nLast Operation: Load Image\nImage URI: $textTargetURI"
                Info!!.text = info
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            Toast.makeText(
                this@ImageEncryptDecryptActivity,
                "Image loaded Successfully",
                Toast.LENGTH_SHORT
            )
                .show()
        }
        // Update UI to reflect image being shared

    }

    /** IMAGE LOADER*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val targetURI = data!!.data
            textTargetURI = targetURI.toString()
            try {
                bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(targetURI!!))
                bitmapCopy = bitmap!!.copy(Bitmap.Config.ARGB_8888, true)
                targetImage!!.setImageBitmap(bitmap)
                height = bitmap!!.getHeight()
                width = bitmap!!.getWidth()
                val info =
                    "Height = $height pixels\nWidth = $width pixels\nLast Operation: Load Image\nImage URI: $textTargetURI"
                Info!!.text = info
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            Toast.makeText(
                this@ImageEncryptDecryptActivity,
                "Image loaded Successfully",
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }


    /** refresh the gallery  */
    private fun addImageGallery(file: File) {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.DATA, file.absolutePath)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg") // setar isso
        contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    }
    /* Request app user to allow the needed runtime permission.
       It will popup a confirm dialog , user can click allow or deny. */

    private fun requestRuntimePermission(
        activity: Activity,
        runtimePermission: String,
        requestCode: Int
    ) {
        ActivityCompat.requestPermissions(activity, arrayOf(runtimePermission), requestCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == this.PERMISSION_REQUEST_CODE) {
            if (grantResults.size > 0) {
                // Construct result message.
                val msgBuf = StringBuffer()
                val grantResult = grantResults[0]
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    //msgBuf.append("You granted below permissions, you can do the action again to use the permission : ");
                } else {
                    msgBuf.append("You denied below permissions : ")
                }

                // Add granted permissions to the message.
                if (permissions != null) {
                    val length = permissions.size
                    for (i in 0 until length) {
                        val permission = permissions[i]
                        msgBuf.append(permission)
                        if (i < length - 1) {
                            msgBuf.append(",")
                        }
                    }
                }
                // Show result message.
                Toast.makeText(applicationContext, msgBuf.toString(), Toast.LENGTH_SHORT).show()
            }
        }

    }


    /** PERMISSION STUFF */ // This method is used to check whether current app has required runtime permission.
    private fun hasRuntimePermission(context: Context, runtimePermission: String): Boolean {
        var ret = false

        // Get current android os version.
        val currentAndroidVersion = Build.VERSION.SDK_INT

        // Build.VERSION_CODES.M's value is 23.
        if (currentAndroidVersion > Build.VERSION_CODES.M) {
            // Only android version 23+ need to check runtime permission.
            if (ContextCompat.checkSelfPermission(
                    context,
                    runtimePermission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                ret = true
            }
        } else {
            ret = true
        }
        return ret
    }
}