package com.example.videomodule.encryption.imageEncryptAndDecrypt

import android.content.Context
import android.graphics.Bitmap
import java.lang.Exception
import java.util.*

/*
      * Created by Chetu..
  */

object ImageEncrypter {
    var pixel: Int = 0
    var pixel2: Int = 0
    var temp: Int = 0
    var bitmapCopy: Bitmap? = null
    var randomHeight: Int = 0
    var randomWidth: Int = 0

    @Throws(Exception::class)
    fun ImageEncrypt(context: Context, seed: Int, bound: Int, height: Int, width: Int, bitmapCopy: Bitmap?): Bitmap? {

        val r = Random(seed.toLong())
        for (h in 1 until height) {
            for (w in 1 until width) {
                randomHeight = height * r.nextInt(bound) / bound
                randomWidth = width * r.nextInt(bound) / bound
                pixel = bitmapCopy!!.getPixel(w, h)
                pixel2 = bitmapCopy!!.getPixel(randomWidth, randomHeight)
                temp = pixel
                pixel = pixel2
                pixel2 = temp
                bitmapCopy!!.setPixel(w, h, pixel)
                bitmapCopy!!.setPixel(randomWidth, randomHeight, pixel2)
            }
        }
        return bitmapCopy
    }

}