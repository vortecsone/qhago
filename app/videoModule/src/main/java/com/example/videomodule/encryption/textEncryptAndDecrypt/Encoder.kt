package com.example.videomodule.encryption.textEncryptAndDecrypt

import android.content.Context
import android.util.Base64
import java.lang.Exception
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
/*
      * Created by Chetu..
  */

object Encoder {

    var AES = "AES"
    @Throws(Exception::class)
     fun encrypt(context: Context, Data: String, password: String): String {
        val key = generateKey(password)
        val c = Cipher.getInstance(AES)
        c.init(Cipher.ENCRYPT_MODE, key)
        val encVal = c.doFinal(Data.toByteArray())
        return Base64.encodeToString(encVal, Base64.DEFAULT).toString()
    }

    @Throws(Exception::class)
     private  fun generateKey(password: String): SecretKeySpec {
        val digest = MessageDigest.getInstance("SHA-256")
        val bytes = password.toByteArray(charset("UTF-8"))
        digest.update(bytes, 0, bytes.size)
        val key = digest.digest()
        return SecretKeySpec(key, "AES")
    }


}