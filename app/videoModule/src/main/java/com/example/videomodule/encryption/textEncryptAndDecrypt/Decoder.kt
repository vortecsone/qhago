package  com.example.videomodule.encryption.textEncryptAndDecrypt

import android.content.Context
import android.util.Base64
import java.lang.Exception
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

  /*
      * Created by Chetu..
  */

object Decoder {
    var AES = "AES"

    @Throws(Exception::class)
     fun decrpt(context: Context,outputsString: String, password: String): String {
        val key = generateKey(password)
        val c = Cipher.getInstance(AES)
        c.init(Cipher.DECRYPT_MODE, key)
        val decoderValue =
            Base64.decode(outputsString, Base64.DEFAULT)
        val decValue = c.doFinal(decoderValue)
        return String(decValue)
    }

    @Throws(Exception::class)
    private fun generateKey(password: String): SecretKeySpec {
        val digest = MessageDigest.getInstance("SHA-256")
        val bytes = password.toByteArray(charset("UTF-8"))
        digest.update(bytes, 0, bytes.size)
        val key = digest.digest()
        return SecretKeySpec(key, "AES")
    }
}