package com.example.que_hago

import android.content.Intent
import android.os.Bundle
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.rol_choosing_content_scrolling.*

class RolChoosingActivity : AppCompatActivity()
{
    var compositeDisposable = CompositeDisposable()
    var userID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rol_choosing)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        getExtras()

        btnAsCivil.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            intent.putExtra("userType","client")
            startActivity(intent)
        }

        btnAsLawyer.setOnClickListener {
            val intent = Intent(this, LawyerBankAccountActivity::class.java)
            intent.putExtra("userID",userID)
            startActivity(intent)
        }
    }

    private fun getExtras()
    {
        userID = intent.getIntExtra("userID",0)
    }

    override fun onStop()
    {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy()
    {
        compositeDisposable.clear()
        super.onDestroy()
    }




}