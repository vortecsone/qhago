package com.example.que_hago.nearByLawyer.utils

interface GpsEnableListener {
    fun onPositive()
}