package com.example.que_hago.nearByLawyer.viewmodel

import android.accounts.NetworkErrorException
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mapmodule.nearbyLawer.model.PolylineData
import com.example.que_hago.R
import com.example.que_hago.nearByLawyer.app.MyApplication
import com.example.que_hago.nearByLawyer.model.AllLawyerResponse
import com.example.que_hago.nearByLawyer.model.LawyerDetailsModelResponse

import com.example.que_hago.nearByLawyer.model.LawyerDetailsModelResponseItem
import com.example.que_hago.nearByLawyer.network.NetworkState
import com.example.que_hago.nearByLawyer.repository.AppRepository
import com.example.que_hago.nearByLawyer.repository.PolyLineUseCase
import com.example.que_hago.nearByLawyer.utils.NonNullMediatorLiveData
import com.example.que_hago.nearByLawyer.utils.Resource
import com.example.que_hago.nearByLawyer.utils.Utils.hasInternetConnection
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

class AllLawyerViewModel(
    app: Application,
    private val appRepository: AppRepository,
    private val polyLineUseCase: PolyLineUseCase,
) : AndroidViewModel(app) {

    val lawyerData: MutableLiveData<Resource<AllLawyerResponse>> = MutableLiveData()
    val lawyerPersonalDetails: MutableLiveData<Resource<LawyerDetailsModelResponse>> =
        MutableLiveData()

    init {
        getAllLawyers()
        // getLawyerDetails()
    }

    fun getAllLawyers() = viewModelScope.launch {
        fetchAllLawyers()
    }

    fun getLawyerDetails(id: Int) = viewModelScope.launch {
        fetchLawyerDetails(id)
    }

    private suspend fun fetchLawyerDetails(
        id: Int
    ) {
        lawyerPersonalDetails.postValue(Resource.Loading())
        try {
            if (hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getLawyersDetails(id)
                lawyerPersonalDetails.postValue(handleLawyerDetailsResponse(response))
            } else {
                lawyerPersonalDetails.postValue(
                    Resource.Error(
                        getApplication<MyApplication>().getString(
                            R.string.no_internet_connection
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            when (t) {


                is SocketTimeoutException -> lawyerPersonalDetails.postValue(
                    Resource.Error(
                        getApplication<MyApplication>().getString(
                            R.string.socket_failure
                        )
                    )
                )
                is NetworkErrorException -> {
                    lawyerPersonalDetails.postValue(
                        Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        )
                    )
                }
                is IOException -> lawyerPersonalDetails.postValue(
                    handleLawyerDetailsResponse(
                        appRepository.getLawyersDetails(id)
                    )
                )

                else ->
                    lawyerPersonalDetails.postValue(
                        Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        )
                    )
            }
        }
    }

    fun getPolyLineData(
        markerLatLng: LatLng,
        currentLatLng: LatLng
    ): NonNullMediatorLiveData<NetworkState<List<PolylineData>>> {

        val polyLineList = NonNullMediatorLiveData<NetworkState<List<PolylineData>>>()

        viewModelScope.launch {
            polyLineUseCase.executeQuery(
                polyLineList,
                markerLatLng,
                currentLatLng
            )
        }

        return polyLineList
    }


    private suspend fun fetchAllLawyers() {
        lawyerData.postValue(Resource.Loading())
        try {
            if (hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getAllLawyers()
                lawyerData.postValue(handleLawyerResponse(response))
            } else {
                lawyerData.postValue(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection)))
            }
        } catch (t: Throwable) {
            when (t) {


                is SocketTimeoutException -> lawyerData.postValue(
                    Resource.Error(
                        getApplication<MyApplication>().getString(
                            R.string.socket_failure
                        )
                    )
                )
                is NetworkErrorException -> {
                    lawyerData.postValue(
                        Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        )
                    )
                }
                is IOException -> lawyerData.postValue(handleLawyerResponse(appRepository.getAllLawyers()))

//                else ->
//                    lawyerData.postValue(
//                        Resource.Error(
//                            getApplication<MyApplication>().getString(
//                                R.string.conversion_error
//                            )
//                        )
//                    )
            }
        }
    }

    private fun handleLawyerResponse(response: Response<AllLawyerResponse>): Resource<AllLawyerResponse> {
        if (response.isSuccessful) {

            response.body()?.let { resultResponse ->

                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

    private fun handleLawyerDetailsResponse(response: Response<LawyerDetailsModelResponse>): Resource<LawyerDetailsModelResponse> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }


}