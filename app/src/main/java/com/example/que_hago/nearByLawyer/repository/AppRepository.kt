package com.example.que_hago.nearByLawyer.repository

import com.example.que_hago.nearByLawyer.network.RetrofitInstance

class AppRepository {
    suspend fun getAllLawyers() = RetrofitInstance.lawyerAPI.getAllLawyers()
    suspend fun getLawyersDetails(id: Int) = RetrofitInstance.lawyerAPI.getLawyerDataByID(id)

    suspend fun getPolyLineData(url: String) =
        RetrofitInstance.polyLineAPI.getPolylineDataAsync(url).await()

}