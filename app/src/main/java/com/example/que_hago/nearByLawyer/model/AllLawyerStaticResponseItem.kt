package com.example.que_hago.nearByLawyer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllLawyerStaticResponseItem(
    val full_name: String,
    val is_lawyer: Boolean,
):Parcelable