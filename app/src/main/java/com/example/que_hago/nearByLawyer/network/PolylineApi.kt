package com.example.que_hago.nearByLawyer.network

import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Url
/*
      * Created by Chetu..
  */

interface PolylineApi {
    @GET
    fun getPolylineDataAsync(@Url url : String) : Deferred<JsonObject>
}