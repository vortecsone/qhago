package com.example.que_hago.nearByLawyer.utils

object Constants {
    const val BASE_URL = "http://50.192.137.250:3000/"
//    const val BASE_URL = "http://45.225.92.140:3000/"
    const val GPS_REQUEST_LOCATION = 1000
    const val TIMEOUT_REQUEST: Long = 30
    const val REQUEST_DENIED = 403

    /* Google direction api Tags */

    const val FORMAT_TAG = "json"

    const val ORIGIN_TAG = "origin="

    const val DESTINATION_TAG = "destination="

    const val SENSOR_TAG = "sensor=false"

    const val KEY_TAG = "key="

    /* Google direction api Response Tags */

    const val STATUS_TAG = "status"

    const val OK_TAG = "OK"

    const val DISTANCE_TAG = "distance"
    const val DURATION_TAG = "duration"

    const val TEXT_TAG = "text"

    const val ROUTES_TAG = "routes"

    const val LEGS_TAG = "legs"

    const val STEPS_TAG = "steps"

    const val POLYLINE_TAG = "polyline"

    const val POINTS_TAG = "points"

    const val LAT_TAG = "lat"

    const val LNG_TAG = "lng"

    /* Notification */

    const val CHANNEL_ID = "id"

    const val CHANNEL_NAME = "an"

    const val KM = " km"
    const val DISTANCE = "Distance: "
    const val DURATION = "Duration: "

    const val DIRECTION_API_URL = "https://maps.googleapis.com/maps/api/directions/"

    // Field from build type: debug
    const val MAP_API_kEY = "AIzaSyDMRgdaCjgoS-2it8yiUE7KZsW1s7D7cuw"

    // Field from build type: debug
    const val MAP_BASE_URL = "https://maps.googleapis.com/maps/api/directions/"
    const val LAWYER_NAME = "LawyerName"
    const val LAWYER_TITLE = "Lawyer"
    const val USER_TITLE = "User"
    const val USER_ID = "userID"


}