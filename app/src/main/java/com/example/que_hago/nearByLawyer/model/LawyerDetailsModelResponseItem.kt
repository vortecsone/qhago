package com.example.que_hago.nearByLawyer.model

data class LawyerDetailsModelResponseItem(
    val email: String,
    val facial_id: Any,
    val full_name: String,
    val hour_price: Int,
    val id: Int,
    val rut: String
)