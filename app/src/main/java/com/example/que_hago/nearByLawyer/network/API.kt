package com.example.que_hago.nearByLawyer.network

import com.example.que_hago.nearByLawyer.model.AllLawyerResponse
import com.example.que_hago.nearByLawyer.model.LawyerDetailsModelResponse
import com.example.que_hago.nearByLawyer.model.LawyerDetailsModelResponseItem
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface API {
    @POST("getAlluserLocation")
    suspend fun getAllLawyers(): Response<AllLawyerResponse>

    @FormUrlEncoded
    @POST( "getLawyerDataByID")
   suspend fun getLawyerDataByID(@Field("lawyerID") lawyerId:Int):Response<LawyerDetailsModelResponse>
}
