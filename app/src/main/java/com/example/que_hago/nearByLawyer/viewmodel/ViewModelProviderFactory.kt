package com.example.que_hago.nearByLawyer.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.que_hago.nearByLawyer.repository.AppRepository
import com.example.que_hago.nearByLawyer.repository.PolyLineUseCase
import com.example.que_hago.nearByLawyer.utils.UiHelper

class ViewModelProviderFactory(
    val app: Application,
    val appRepository: AppRepository,
    val uiHelper: UiHelper? = null
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(AllLawyerViewModel::class.java)) {
            return AllLawyerViewModel(app, appRepository, PolyLineUseCase(appRepository)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}