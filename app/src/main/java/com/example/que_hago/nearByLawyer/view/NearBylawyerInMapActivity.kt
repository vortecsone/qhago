package com.example.que_hago.nearByLawyer.view

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mapmodule.nearbyLawer.model.PolylineData
import com.example.que_hago.MainActivity
import com.example.que_hago.R
import com.example.que_hago.nearByLawyer.adapter.LawyerDetailsAdapter
import com.example.que_hago.nearByLawyer.model.AllLawyerResponseItem
import com.example.que_hago.nearByLawyer.network.NetworkState
import com.example.que_hago.nearByLawyer.repository.AppRepository
import com.example.que_hago.nearByLawyer.utils.*
import com.example.que_hago.nearByLawyer.utils.Constants.GPS_REQUEST_LOCATION
import com.example.que_hago.nearByLawyer.utils.Constants.KM
import com.example.que_hago.nearByLawyer.utils.Constants.LAT_TAG
import com.example.que_hago.nearByLawyer.utils.Constants.LNG_TAG
import com.example.que_hago.nearByLawyer.utils.Constants.REQUEST_DENIED
import com.example.que_hago.nearByLawyer.viewmodel.AllLawyerViewModel
import com.example.que_hago.nearByLawyer.viewmodel.ViewModelProviderFactory
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.mayowa.android.locationwithlivedata.LocationViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_near_bylawyer_in_map.*
import kotlinx.android.synthetic.main.layout_event.*
import retrofit2.Retrofit
import java.lang.Double
import kotlin.Array
import kotlin.Boolean
import kotlin.Int
import kotlin.IntArray
import kotlin.String
import kotlin.Throwable
import kotlin.also
import kotlin.arrayOf
import kotlin.let
import kotlin.plus
import kotlin.toString

 class NearBylawyerInMapActivity : AppCompatActivity(), OnMapReadyCallback,
    PermissionHelper.OnPermissionRequested, GoogleMap.OnMarkerClickListener {

    private lateinit var viewModel: AllLawyerViewModel
    lateinit var lawyerAdapter: LawyerDetailsAdapter
    private var lat: kotlin.Double = 0.0
    private var longi: kotlin.Double = 0.0

    private var lat1: kotlin.Double = 0.0
    private var long1: kotlin.Double = 0.0
    private var lat2: kotlin.Double = 0.0
    private var long2: kotlin.Double = 0.0

    private val options = MarkerOptions()
    private lateinit var googleMapHelper: GoogleMapHelper
    private var gpsSetting: GpsSetting? = null
    private var uiHelper: UiHelper? = null
    private var permissionHelper: PermissionHelper? = null

    private var isPermissionPermanentlyDenied = false
    private var markerLatLng: LatLng? = null

    private var distances: String = ""
    private var durations: String = ""
    private lateinit var locationViewModel: LocationViewModel


    private var mMap: GoogleMap? = null

    // below are the latitude and longitude
    // of 4 different locations.
    var sydney = LatLng(28.6311240, 77.373680)
    var TamWorth = LatLng(28.691240, 77.37480)
    var NewCastle = LatLng(28.7801220, 77.378480)
    var Brisbane = LatLng(28.7801220, 77.478480)

    // creating array list for adding all our locations.
    private var locationArrayList: ArrayList<LatLng>? = null
    private var lawyerArrayList: ArrayList<String>? = null
    private var lawyerID: ArrayList<Int>? = null
    private val listLatLng = ArrayList<LatLng>()

    //    private val eventsImagesAdapter = LawyerDetailsAdapter()
    private var bottomSheet: LinearLayout? = null

    private var eventsPB: ProgressBar? = null
    private var blackPolyLine: Polyline? = null
    private var greyPolyLine: Polyline? = null

    private lateinit var marker: Marker
    private var isLocationEnabled = false

    @RequiresApi(Build.VERSION_CODES.M)
    private var latlng: LatLng = LatLng(0.0, 0.0)

    lateinit var myAPI: INodeJS
     var userId: Int? = 0


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_near_bylawyer_in_map)


        init()
        locationPermission()
        // in below line we are initializing our array list.
        locationArrayList = ArrayList()
        lawyerArrayList = ArrayList()
        lawyerID = ArrayList()
        userId = intent.getIntExtra("userID", 0)
        val retrofit: Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)

        // locations in our array list.
//        locationArrayList!!.add(sydney)
//        locationArrayList!!.add(TamWorth)
//        locationArrayList!!.add(NewCastle)
//        locationArrayList!!.add(Brisbane)


// …
// Before you perform the actual permission request, check whether your app
// already has the permissions, and whether your app needs to show a permission
// rationale dialog. For more details, see Request permissions.

    }

    private fun locationPermission() {
        if (ContextCompat.checkSelfPermission(
                this@NearBylawyerInMapActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@NearBylawyerInMapActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    this@NearBylawyerInMapActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    this@NearBylawyerInMapActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun init() {
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        setupViewModel()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray


    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    if ((ContextCompat.checkSelfPermission(
                            this@NearBylawyerInMapActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) ==
                                PackageManager.PERMISSION_GRANTED)
                    ) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }


    /**
     * This function is to get the result form [PermissionHelper] class
     *
     * @param isPermissionGranted the [Boolean]
     */


    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupViewModel() {
        val repository = AppRepository()
        googleMapHelper = GoogleMapHelper()
        uiHelper = UiHelper(this)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)

        val factory = ViewModelProviderFactory(application, repository)
        viewModel = ViewModelProvider(this, factory).get(AllLawyerViewModel::class.java)
        permissionHelper = PermissionHelper(this, uiHelper!!)
        getLawyers()
//        getStaticLawyers()
    }

    private fun getStaticLawyers() {
        val data = ArrayList<AllLawyerResponseItem>()
        // the image with the count of view
//        data.add(
//            AllLawyerResponseItem(70, "dasd dasd", 1, "28.6311240", "77.373680", "")
//        )
//        data.add(
//            AllLawyerResponseItem(51, "Test Test", 1, "28.6321240", "77.378580", "")
//        )
//        data.add(
//            AllLawyerResponseItem(71, "dasd dasd", 1, "28.6301220", "77.378480", "")
//        )
//        data.add(
//            AllLawyerResponseItem(51, "eqwqw eqweq", 1, "28.584670", "29.584670", "")
//        )


//        for (i in locationArrayList!!.indices) {
//
//            // below line is use to add marker to each location of our array list.
//            mMap!!.addMarker(
//                MarkerOptions().position(locationArrayList!![i]).title(Constants.LAWYER_TITLE)
//            )?.showInfoWindow()
//
//            // below lin is use to zoom our camera on map.
////            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(16.0f))
//
//            // below line is use to move our camera to the specific location.
//            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList!![i]))
//        }


//        currentLatLng = LatLng(data.get(0).latitude.toDouble(),data.get(0).longitude.toDouble())
//        val result=ArrayList<PolylineData>()
//        for (i in result.indices) {

//            val routeInfoData = result[i].routeInfo
//            distances = routeInfoData.distance.replace(KM, "")
//            durations = routeInfoData.duration
//            }


        // This will pass the ArrayList to our Adapter
        lawyerAdapter = LawyerDetailsAdapter(data.distinctBy {
            it.full_name

        }, object : OnLawyerItemClickListener {

            override fun onItemClick(item: AllLawyerResponseItem) {
//                getLawyerData(item)


            }

        })

        // Setting the Adapter with the recyclerview
        recyclerView?.adapter = lawyerAdapter
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun getLawyers() {
        viewModel.lawyerData.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    checkPermissionGranted()

                    response.data?.let {

                        val list = it.filter { it.is_lawyer == 1 }

                        lawyerAdapter =
                            LawyerDetailsAdapter(list.distinctBy {
                                it.full_name
                                // TODO()
                            }, object : OnLawyerItemClickListener {
                                override fun onItemClick(item: AllLawyerResponseItem) {

                                    getLawyerData(item)

                                }

                            })

                        recyclerView?.adapter = lawyerAdapter
                        for (i in list.distinctBy { it.full_name }) {
                            locationArrayList?.add(
                                LatLng(
                                    i.latitude.toDouble(),
                                    i.longitude.toDouble()
                                )
                            )
                            lat2 = locationArrayList?.get(0)?.latitude!!.toDouble()
                            long2 = locationArrayList?.get(0)?.longitude!!.toDouble()
                            lawyerArrayList?.add((i.full_name))
                            lawyerID?.add(i.id)
                        }
                        lat1 = lat
                        long1 = longi
                        val dis = distanceInKm(lat1, longi, lat2, long2)
                    }


                }

                is Resource.Error -> {
                    hideProgressBar()
                    checkPermissionGranted()
                    response.message?.let { message ->
                        rootLayout.errorSnack(message, Snackbar.LENGTH_LONG)

//                        lawyerAdapter.differ.submitList(response.data)

                    }

                }

                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getLawyerData(item: AllLawyerResponseItem) {
        viewModel.getLawyerDetails(item.id)

        viewModel.lawyerPersonalDetails.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    checkPermissionGranted()

                    response.data?.let {
//                        TODO
                        val facilaId = it.get(0).facial_id
                        val userId = it.get(0).id


                        Intent(
                            this@NearBylawyerInMapActivity,
                            MainActivity::class.java
                        ).also {
                            it.putExtra(Constants.LAWYER_NAME, item.full_name)
                            it.putExtra(Constants.USER_ID, userId.toString())
//                            it.putExtra(Constants.USER_ID, facilaId.toString())
                            startActivity(it)
                        }

                    }
                }

                is Resource.Error -> {
                    hideProgressBar()
                    checkPermissionGranted()
                    response.message?.let { message ->
                        rootLayout.errorSnack(message, Snackbar.LENGTH_LONG)

//                        lawyerAdapter.differ.submitList(response.data)

                    }

                }

                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

    }


    private fun handleError(error: Throwable) {
        Log.d("type", error.localizedMessage)
    }

    private fun distanceInKm(
        lat1: kotlin.Double,
        lon1: kotlin.Double,
        lat2: kotlin.Double,
        lon2: kotlin.Double
    ): kotlin.Double {
        val theta = lon1 - lon2
        var dist =
            Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(
                deg2rad(lat2)
            ) * Math.cos(deg2rad(theta))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist *= 60 * 1.1515
        dist *= 1.609344
        return dist

    }

    private fun deg2rad(deg: kotlin.Double): kotlin.Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: kotlin.Double): kotlin.Double {
        return rad * 180.0 / Math.PI
    }

    private fun hideProgressBar() {
        events_pb.visibility = View.GONE
    }

    private fun showProgressBar() {
        events_pb.visibility = View.VISIBLE
    }

    /** Called when the map is ready. */
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        mMap?.setOnMarkerClickListener(this)
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

        for (i in locationArrayList!!.indices) {

            // below line is use to add marker to each location of our array list.
            mMap!!.addMarker(
                MarkerOptions().position(locationArrayList!![i]).title(lawyerArrayList!![i])
            )?.showInfoWindow()
//    mMap!!.addMarker(
//                MarkerOptions().position(locationArrayList!![i]).title(Constants.LAWYER_TITLE)
//            )?.showInfoWindow()

            // below lin is use to zoom our camera on map.
//            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(16.0f))

            // below line is use to move our camera to the specific location.
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList!![i]))

            val idLawyers = lawyerID?.get(i)
        }

        mMap?.addMarker(MarkerOptions().position(latlng).title(Constants.USER_TITLE))
            ?.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))

//        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(16.0f))
//        mMap?.animateCamera(CameraUpdateFactory.newLatLng(LatLng(29.0588,76.0856)))
        mMap?.animateCamera(CameraUpdateFactory.newLatLng(latlng))
//        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 11.0f))

        mMap?.setOnInfoWindowClickListener {
            val mLawyerName = it.title
            Intent(
                this@NearBylawyerInMapActivity,
                MainActivity::class.java
            ).also {
                it.putExtra(Constants.LAWYER_NAME, mLawyerName)
//                            it.putExtra(Constants.USER_ID, facilaId.toString())
                startActivity(it)
            }

        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMarkerClick(marker: Marker): Boolean {
        markerLatLng =
            googleMapHelper.getLatLng(marker.position.latitude, marker.position.longitude)
        drawPloyLine()
        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun drawPloyLine() {
        if (latlng != markerLatLng) {
            clearPloyLineAnimation()

            if (uiHelper!!.getConnectivityStatus())
                subscribePolyLineObserver(markerLatLng!!, latlng)
            else
                uiHelper?.toast(resources.getString(R.string.network_failure))
        }
    }

    private fun subscribePolyLineObserver(markerLatLng: LatLng, currentLatLng: LatLng) =
        // OBSERVABLES ---
        viewModel.getPolyLineData(markerLatLng, currentLatLng).nonNull().observe(this,
            Observer {
                when (it) {
                    is NetworkState.Loading -> eventsPB?.let { data ->
                        uiHelper?.showProgressBar(
                            data,
                            true
                        )
                    }
                    is NetworkState.Success -> {
                        if (it.data != null) {
                            eventsPB?.let { data -> uiHelper?.showProgressBar(data, false) }
                            drawPolyline(it.data)
                        }
                    }
                    is NetworkState.Error -> {
                        eventsPB?.let { data -> uiHelper?.showProgressBar(data, false) }
                        if (it.code == REQUEST_DENIED)
                            uiHelper?.toast(resources.getString(R.string.getting_route))
                    }
                }
            })

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onStart() {
        super.onStart()
        if (locationArrayList!!.isNotEmpty()) {
            checkPermissionGranted()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        clearPloyLineAnimation()
    }
    /*
     * Checking whether Location Permission is granted or not.
     * */

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkPermissionGranted() {
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) permissionHelper?.openSettingsDialog()
        else {
            enableGps()
        }
    }


    /**
     * This function is to get the result form [PermissionHelper] class
     *
     * @param isPermissionGranted the [Boolean]
     */

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onPermissionResponse(isPermissionGranted: Boolean) {

        if (!isPermissionGranted) {
            isPermissionPermanentlyDenied = true
        } else {
            enableGps()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun enableGps() {
        isPermissionPermanentlyDenied = false
        if (!uiHelper!!.isLocationProviderEnabled()) updateLocation()
//        if (!uiHelper!!.isLocationProviderEnabled()) subscribeLocationObserver()
        else gpsSetting?.openGpsSettingDialog()
    }


    // Start Observing the User Current Location and set the marker to it.

    @RequiresApi(Build.VERSION_CODES.M)
    private fun updateLocation() {

        locationViewModel.getLocationData().observe(this, Observer {
            latlng = LatLng(it.latitude, it.longitude)

            lat = it.latitude
            longi = it.longitude
            if (!isLocationEnabled) {
                // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                val mapFragment: SupportMapFragment? =
                    supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                mapFragment!!.getMapAsync(this)
                isLocationEnabled = true
            }
//            Toast.makeText(this, "Latitude " + lat + "\nLongitude " + longi, Toast.LENGTH_LONG).show()

        })
    }

//    private fun mapSetUp(latLing: LatLng) {
//        if (googleMap != null) {
//            googleMap?.addMarker(latLing?.let { googleMapHelper.addCurrentLocationMarker(it) })?.title =
//                "Client"
//            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLing, 16.0F))
//
//            val markerList = ArrayList<LatLng>()
//
//            /*
//              * list of neraby lawyer...
//            */
//            markerList.add(LatLng(28.6311240, 77.373680))
//            markerList.add(LatLng(28.6321240, 77.378580))
//            markerList.add(LatLng(28.6301220, 77.378480))
//            markerList.add(LatLng(28.6301340, 77.378670)) //some latitude and logitude value
//
//            for (point in markerList) {
//                options.position(point)
//                options.title("Lawyer")
//                googleMap!!.addMarker(options)
//                    .setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
//            }
//
//            if (uiHelper!!.getConnectivityStatus()) {
//
//            }
//            //TODO
////            else uiHelper.showSnackBar(event_activity_rv,resources.getString(R.string.error_network_connection))
//        }
//
//
//    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            GPS_REQUEST_LOCATION ->
                when (resultCode) {
                    RESULT_OK -> updateLocation()
//                    RESULT_OK -> subscribeLocationObserver()

                    RESULT_CANCELED -> {
                        uiHelper?.showPositiveDialogWithListener(
                            this,
                            resources.getString(R.string.need_location),
                            resources.getString(R.string.location_content),
                            object : GpsEnableListener {
                                override fun onPositive() {
                                    enableGps()
                                }
                            }, resources.getString(R.string.turn_on), false
                        )
                    }
                }
        }
    }

    private fun drawPolyline(result: List<PolylineData>) {
        val points: ArrayList<LatLng> = ArrayList()
        // Traversing through all the routes
        for (i in result.indices) {
            val routeInfoData = result[i].routeInfo
//             distance: String = routeInfoData.distance.replace(KM, "")
            distances = routeInfoData.distance
            durations = routeInfoData.duration
            distance.text = distances
//            if (distances.toFloat() <= 4.9)
            createLawyerDistanceAndDuration(distances)

            // Fetching i-th route
            val path = result[i].ployLineRoutesList

            // Fetching all the points in i-th route
            for (j in path?.indices!!) {
                val point = path[j]

                val lat = point[LAT_TAG]?.let { Double.parseDouble(it) }
                val lng = point[LNG_TAG]?.let { Double.parseDouble(it) }
                val position = lat?.let { lng?.let { data -> LatLng(it, data) } }

                position?.let { points.add(it) }
            }

            this.listLatLng.addAll(points)
        }


        blackPolyLine = mMap!!.addPolyline(googleMapHelper.getPolyLineOptions(Color.BLACK))
        greyPolyLine = mMap!!.addPolyline(googleMapHelper.getPolyLineOptions(Color.GRAY))



        if (blackPolyLine != null && greyPolyLine != null && listLatLng.size >= 1)
            googleMapHelper.animatePolyLine(blackPolyLine, greyPolyLine, listLatLng)
    }

    private fun createLawyerDistanceAndDuration(distances: String) {
        val message: String =
            this?.resources?.getString(R.string.lawyer_distance) + " " + distances + KM + "\nTime Duration " + durations
        distance?.setText("" + message)

    }


    private fun clearPloyLineAnimation() {
        if (blackPolyLine != null && greyPolyLine != null) {
            blackPolyLine?.remove()
            greyPolyLine?.remove()
            this.listLatLng.clear()
        }
    }


//    override fun onBackPressed() {
//        val intent: Intent = Intent(
//            this@NearBylawyerInMapActivity,
//            NearBylawyerInMapActivity::class.java
//        )
//        startActivity(intent)
//
//    }

}