package com.example.que_hago.nearByLawyer.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.que_hago.MainActivity
import com.example.que_hago.R
import com.example.que_hago.nearByLawyer.adapter.LawyerDetailsAdapter
import com.example.que_hago.nearByLawyer.adapter.LawyerStaticDetailsAdapter
import com.example.que_hago.nearByLawyer.model.AllLawyerResponseItem
import com.example.que_hago.nearByLawyer.model.AllLawyerStaticResponseItem
import com.example.que_hago.nearByLawyer.repository.AppRepository
import com.example.que_hago.nearByLawyer.utils.*
import com.example.que_hago.nearByLawyer.viewmodel.AllLawyerViewModel
import com.example.que_hago.nearByLawyer.viewmodel.ViewModelProviderFactory
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import com.example.que_hago.ui.login.LoginActivity
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.mayowa.android.locationwithlivedata.LocationViewModel
import kotlinx.android.synthetic.main.activity_map_box.*
import kotlinx.android.synthetic.main.layout_event.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class MapBoxActivity : AppCompatActivity(), OnMapReadyCallback, PermissionsListener,
    MapboxMap.OnMarkerClickListener {

    private val REQUEST_CODE_AUTOCOMPLETE = 7171
    private lateinit var mapboxMap: MapboxMap
    private var permissionsManager: PermissionsManager? = null
    private var locationComponent: LocationComponent? = null
    private var currentRoute: DirectionsRoute? = null
    private val TAG = "DirectionsActivity"
    private var navigationMapRoute: NavigationMapRoute? = null
    /********* New Part Search *********/
    private val geoJsonSourceLayerId = "GeoJsonSourceLayerId"


    private lateinit var viewModel: AllLawyerViewModel
    lateinit var lawyerAdapter1: LawyerStaticDetailsAdapter

//    TODO
    lateinit var lawyerAdapter: LawyerDetailsAdapter

    private var lat: kotlin.Double = 0.0
    private var longi: kotlin.Double = 0.0

    private var lat1: kotlin.Double = 0.0
    private var long1: kotlin.Double = 0.0
    private var lat2: kotlin.Double = 0.0
    private var long2: kotlin.Double = 0.0

    private val options = MarkerOptions()
    private lateinit var googleMapHelper: GoogleMapHelper
    private var gpsSetting: GpsSetting? = null
    private var uiHelper: UiHelper? = null
    private var permissionHelper: PermissionHelper? = null
    private var markerLatLng: LatLng? = null
    private var distances: String = ""
    private var durations: String = ""
    private lateinit var locationViewModel: LocationViewModel
    private lateinit var callback: LocationChangeListeningCallback


    // creating array list for adding all our locations.
    private var locationArrayList: ArrayList<LatLng>? = null
    private var lawyerArrayList: ArrayList<String>? = null
    private var lawyerID: ArrayList<Int>? = null


    @RequiresApi(Build.VERSION_CODES.M)
    private var latlng: LatLng = LatLng(0.0, 0.0)

    lateinit var myAPI: INodeJS
    var userId: Int? = 0
    private var symbolManager: SymbolManager? = null

    private val symbolIconId = "SymbolIconId"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.access_token))
        setContentView(R.layout.activity_map_box)
        mapView!!.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        addAnnotationToMap()
        init()
        // in below line we are initializing our array list.
        locationArrayList = ArrayList()
        lawyerArrayList = ArrayList()
        lawyerID = ArrayList()
        userId = intent.getIntExtra("userID", 0)
        val retrofit: Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)



    }



    private fun addAnnotationToMap() {
        // Create an instance of the Annotation API and get the PointAnnotationManager.
        bitmapFromDrawableRes(
            this@MapBoxActivity,
           R.drawable.ic_location_on_red_24dp
        )
    }
    private fun bitmapFromDrawableRes(context: Context, @DrawableRes resourceId: Int) =
        convertDrawableToBitmap(AppCompatResources.getDrawable(context, resourceId))

    private fun convertDrawableToBitmap(sourceDrawable: Drawable?): Bitmap? {
        if (sourceDrawable == null) {
            return null
        }
        return if (sourceDrawable is BitmapDrawable) {
            sourceDrawable.bitmap
        } else {
// copying drawable object to not manipulate on the same reference
            val constantState = sourceDrawable.constantState ?: return null
            val drawable = constantState.newDrawable().mutate()
            val bitmap: Bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth, drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            bitmap
        }
    }
/*
* Implement  API
*/

    @RequiresApi(Build.VERSION_CODES.M)
    private fun init() {
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        setupViewModel()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupViewModel() {
        val repository = AppRepository()
        googleMapHelper = GoogleMapHelper()
        uiHelper = UiHelper(this)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)

        val factory = ViewModelProviderFactory(application, repository)
        viewModel = ViewModelProvider(this, factory).get(AllLawyerViewModel::class.java)
        permissionHelper = PermissionHelper(this, uiHelper!!)
        getLawyers()
//        getStaticLawyers()
    }

    private fun getStaticLawyers() {
        val data = ArrayList<AllLawyerStaticResponseItem>()
        // the image with the count of view
        data.add(
            AllLawyerStaticResponseItem("dasd dasd",true)
        )
        data.add(
            AllLawyerStaticResponseItem("chetu", false)
        )
        data.add(
            AllLawyerStaticResponseItem("Test Test", true)
        )
        data.add(
            AllLawyerStaticResponseItem("dasd dasd", true)
        )
        data.add(
            AllLawyerStaticResponseItem( "eqwqw eqweq", true)
        )
        data.add(
            AllLawyerStaticResponseItem( "dev ", false)
        )


//        for (i in locationArrayList!!.indices) {
//
//            // below line is use to add marker to each location of our array list.
//            mMap!!.addMarker(
//                MarkerOptions().position(locationArrayList!![i]).title(Constants.LAWYER_TITLE)
//            )?.showInfoWindow()
//
//            // below lin is use to zoom our camera on map.
////            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(16.0f))
//
//            // below line is use to move our camera to the specific location.
//            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList!![i]))
//        }


//        currentLatLng = LatLng(data.get(0).latitude.toDouble(),data.get(0).longitude.toDouble())
//        val result=ArrayList<PolylineData>()
//        for (i in result.indices) {
//            val routeInfoData = result[i].routeInfo
//            distances = routeInfoData.distance.replace(KM, "")
//            durations = routeInfoData.duration
//            }


        // This will pass the ArrayList to our Adapter
        lawyerAdapter1 = LawyerStaticDetailsAdapter(data.distinctBy {
            it.full_name

        })

        // Setting the Adapter with the recyclerview
        recyclerView?.adapter = lawyerAdapter1
    }
    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        callback = LocationChangeListeningCallback()


        mapboxMap.setStyle(
            Style.MAPBOX_STREETS
        ) {
                style -> enableLocationComponent(style)
             addDestinationIconSymbolLayer(style)

            setUpSource(style!!)

            setupLayer(style)

            val drawable =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_location_on_red_24dp, null)
            val bitmap = BitmapUtils.getBitmapFromDrawable(drawable)
            style.addImage(symbolIconId, bitmap!!)

        }


//        mapboxMap.setStyle(
//            getString(com.mapbox.services.android.navigation.ui.v5.R.string.navigation_guidance_day)
//        ) { style: Style? ->
//
//            enableLocationComponent(style)
//            addDestinationIconSymbolLayer(style)
//
////            btnStart!!.setOnClickListener { v: View? ->
////                val simulateRoute = true
////                val options = NavigationLauncherOptions.builder()
////                    .directionsRoute(currentRoute)
////                    .shouldSimulateRoute(simulateRoute)
////                    .build()
////
////                // Call this method with Context from within an Activity
////                NavigationLauncher.startNavigation(this, options)
////            }
//
//            /************* New Part Search  */
//
////            initSearchFab()
//
////            addUserLocations();
//
//            setUpSource(style!!)
//
//            setupLayer(style)
//
//            val drawable =
//                ResourcesCompat.getDrawable(resources, R.drawable.ic_location_on_red_24dp, null)
//            val bitmap = BitmapUtils.getBitmapFromDrawable(drawable)
//            style.addImage(symbolIconId, bitmap!!)
//
//
//        }


    }




    private inner class LocationChangeListeningCallback :
        LocationEngineCallback<LocationEngineResult> {

        override fun onSuccess(result: LocationEngineResult?) {
            result?.lastLocation ?: return //BECAREFULL HERE, IF NAME LOCATION UPDATE DONT USER -> val resLoc = result.lastLocation ?: return
            if (result.lastLocation != null){
                val lat = result.lastLocation?.latitude!!
                val lng = result.lastLocation?.longitude!!
                val latLng = LatLng(lat, lng)

                if (result.lastLocation != null) {
                    mapboxMap?.locationComponent?.forceLocationUpdate(result.lastLocation)
                    val position = CameraPosition.Builder()
                        .target(latLng)
                        .zoom(13.0) //disable this for not follow zoom
                        .tilt(10.0)
                        .build()
                    mapboxMap?.animateCamera(CameraUpdateFactory.newCameraPosition(position))
                    Toast.makeText(this@MapBoxActivity, "Location update : $latLng", Toast.LENGTH_LONG).show()
                    Log.d("Location update",result.lastLocation?.latitude!!.toString())
                }
            }

        }

        override fun onFailure(exception: Exception) {}
    }


    private fun setupLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer("SYMBOL_LAYER_ID", geoJsonSourceLayerId).withProperties(
                PropertyFactory.iconImage(symbolIconId),
                PropertyFactory.iconOffset(arrayOf(0f, -8f))
            )
        )
    }

    private fun setUpSource(loadedMapStyle: Style) {
        loadedMapStyle.addSource(GeoJsonSource(geoJsonSourceLayerId))
    }

//    private fun initSearchFab() {
//        fab_location_search.setOnClickListener { v: View? ->
//            val intent = PlaceAutocomplete.IntentBuilder()
//                .accessToken(
//                    (if (Mapbox.getAccessToken() != null) Mapbox.getAccessToken() else getString(
//                        R.string.access_token
//                    ))!!
//                )
//                .placeOptions(
//                    PlaceOptions.builder()
//                        .backgroundColor(Color.parseColor("#EEEEEE"))
//                        .limit(10) //.addInjectedFeature(home)
//                        //.addInjectedFeature(work)
//                        .build(PlaceOptions.MODE_CARDS)
//                )
//                .build(this@MainActivity)
//            startActivityForResult(
//                intent, REQUEST_CODE_AUTOCOMPLETE
//            )
//        }
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

// Retrieve selected location's CarmenFeature
            val selectedCarmenFeature = PlaceAutocomplete.getPlace(data)
            //CarmenFeature carmenFeature = PlacePicker.getPlace(data);

// Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
// Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapboxMap != null) {
                val style = mapboxMap!!.style
                if (style != null) {
                    val source = style.getSourceAs<GeoJsonSource>(geoJsonSourceLayerId)
                    source?.setGeoJson(
                        FeatureCollection.fromFeatures(
                            arrayOf(
                                Feature.fromJson(
                                    selectedCarmenFeature.toJson()
                                )
                            )
                        )
                    )

// Move map camera to the selected location
                    mapboxMap!!.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder()
                                .target(
                                    LatLng(
                                        (selectedCarmenFeature.geometry() as Point?)!!.latitude(),
                                        (selectedCarmenFeature.geometry() as Point?)!!.longitude()
                                    )
                                )
                                .zoom(14.0).tilt(10.0)
                                .build()
                        ), 4000
                    )
                }
            }
        }
    }

    private fun addDestinationIconSymbolLayer(loadedMapStyle: Style?) {
        loadedMapStyle!!.addImage(
            "destination-icon-id", BitmapFactory.decodeResource(
                this.resources,
                com.mapbox.mapboxsdk.places.R.drawable.mapbox_marker_icon_default
            )
        )
        val geoJsonSource = GeoJsonSource("destination-source-id")
        loadedMapStyle.addSource(geoJsonSource)
        val destinationSymbolLayer =
            SymbolLayer("destination-symbol-layer-id", "destination-source-id")
        destinationSymbolLayer.withProperties(
            PropertyFactory.iconImage("destination-icon-id"),
            PropertyFactory.iconAllowOverlap(true),
            PropertyFactory.iconIgnorePlacement(true)
        )
        loadedMapStyle.addLayer(destinationSymbolLayer)
    }

//    override fun onMapClick(point: LatLng): Boolean {
//        val destinationPoint = Point.fromLngLat(point.longitude, point.latitude)
//        val originPoint = Point.fromLngLat(
//            locationComponent!!.lastKnownLocation!!.longitude,
//            locationComponent!!.lastKnownLocation!!.latitude
//        )
//        Toast.makeText(this@MapBoxActivity, "Location update : $originPoint", Toast.LENGTH_LONG).show()
//
//        val source = mapboxMap!!.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
//        source?.setGeoJson(Feature.fromGeometry(destinationPoint))
//
//
//
//        getRoute(originPoint, destinationPoint)
//
////        btnStart!!.isEnabled = true
////        btnStart!!.setBackgroundResource(R.color.mapboxBlue)
//        return true
//    }


    private fun addLawyerMarker(location: LatLng) {
        // Add symbol at specified lat/lon
        val symbol = symbolManager?.create(
            SymbolOptions()
                .withLatLng(location)
                .withIconImage("place-marker")
                .withIconSize(1.0f))

    }
//    override fun onMapClick(point: LatLng): Boolean {
//        val destinationPoint = Point.fromLngLat(point.longitude, point.latitude)
//
//        val originPoint = Point.fromLngLat(
//            locationComponent!!.lastKnownLocation!!.longitude,
//            locationComponent!!.lastKnownLocation!!.latitude
//        )
//        Toast.makeText(this@MapBoxActivity, "Location update : $originPoint", Toast.LENGTH_LONG).show()
//
////        val source = mapboxMap!!.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
////        source?.setGeoJson(Feature.fromGeometry(destinationPoint))
//    getRoute(originPoint, destinationPoint)
//        return false
//    }
    private fun getRoute(origin: Point, destination: Point) {
        NavigationRoute.builder(this).accessToken(Mapbox.getAccessToken()!!)
            .origin(origin)
            .destination(destination)
            .build()
            .getRoute(object : Callback<DirectionsResponse?> {
                override fun onResponse(
                    call: Call<DirectionsResponse?>,
                    response: Response<DirectionsResponse?>
                ) {

                    // You can get the generic HTTP info about the response
                    Log.d(TAG, "Response code: " + response.code())
                    if (response.body() == null) {
                        Log.d(TAG, "No routes found, make sure you set the right user and access token.")
                        return
                    } else if (response.body()!!.routes().size < 1) {
                        Log.e(TAG, "No routes found")
                        return
                    }
                    currentRoute = response.body()!!.routes()[0]

                    // Draw the route on the map
                    if (navigationMapRoute != null) {
                        navigationMapRoute!!.removeRoute()

                    } else {
                        navigationMapRoute = NavigationMapRoute(
                            null,
                            mapView,
                            mapboxMap!!,
                            R.style.NavigationMapRoute
                        )
                    }
                    navigationMapRoute!!.addRoute(currentRoute)
                }

                override fun onFailure(call: Call<DirectionsResponse?>, t: Throwable) {
                    Log.e(TAG, "Error: " + t.message)
                }
            })
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style?) {
        /*Check if permissions are enabled and if not request*/
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap!!.locationComponent

            locationComponent!!.activateLocationComponent(this, loadedMapStyle!!)

            val customLocationComponentOptions = LocationComponentOptions.builder(this)
                .elevation(5F)
                .accuracyAlpha(0.6f)
                .accuracyColor(Color.GRAY)
                .foregroundDrawable(R.drawable.pin)
                .build()

// Get an instance of the component
//            locationComponent = mapboxMap.getLocationComponent()

            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

// Activate with options
            locationComponent!!.activateLocationComponent(locationComponentActivationOptions)
            locationComponent!!.setLocationComponentEnabled(true)



//            if (ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return
//            }





//            locationComponent!!.setLocationComponentEnabled(true)



            //Set the component's camera mode


        // Setup camera position
            val location =  mapboxMap.locationComponent.lastKnownLocation
            if (location != null) {
                val position = CameraPosition.Builder()
                    .target(LatLng(location.latitude, location.longitude))
                    .zoom(15.0) // Sets the zoom
                    .bearing(0.0) // Rotate the camera
                    .tilt(0.0) // Set the camera tilt
                    .build() // Creates a CameraPosition from the builder

                mapboxMap.animateCamera(
                    CameraUpdateFactory
                        .newCameraPosition(position), 1
                )
            }
            // Setup the symbol manager object
            symbolManager = SymbolManager(mapView, mapboxMap, loadedMapStyle)


//            val destinationPoint = Point.fromLngLat(marker.position.longitude,marker.position.latitude)
//
//            val originPoint = Point.fromLngLat(
//                locationComponent!!.lastKnownLocation!!.longitude,
//                locationComponent!!.lastKnownLocation!!.latitude
//            )
//            Toast.makeText(this@MapBoxActivity, "Location update : $originPoint", Toast.LENGTH_LONG).show()
//
////        val source = mapboxMap!!.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
////        source?.setGeoJson(Feature.fromGeometry(destinationPoint))
//            getRoute(originPoint, destinationPoint)

            // add click listeners if desired
            symbolManager?.addClickListener { symbol ->
            val destinationPoint=Point.fromLngLat(symbol.geometry.longitude(),symbol.geometry.latitude())
                val originPoint = Point.fromLngLat(
                locationComponent!!.lastKnownLocation!!.longitude,
                locationComponent!!.lastKnownLocation!!.latitude
            )
                getRoute(originPoint, destinationPoint)
            }
            symbolManager?.addLongClickListener { symbol ->
            }
            // set non-data-driven properties, such as:
            symbolManager?.iconAllowOverlap = true
            symbolManager?.iconTranslate = arrayOf(-4f, 5f)
            symbolManager?.iconRotationAlignment = Property.ICON_ROTATION_ALIGNMENT_VIEWPORT

            val bm: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.redmarker)
            mapboxMap.style?.addImage("place-marker", bm)



        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager!!.requestLocationPermissions(this)
        }
    }



    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap!!.style)
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_SHORT)
                .show()
            finish()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        permissionsManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
    //    TODO
    @RequiresApi(Build.VERSION_CODES.M)
    private fun getLawyers() {
        viewModel.lawyerData.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
//                    checkPermissionGranted()

                    response.data?.let {

                        val list = it.filter { it.is_lawyer == 1 }

                        lawyerAdapter =
                            LawyerDetailsAdapter(list.distinctBy {
                                it.full_name
                                // TODO()
                            }, object : OnLawyerItemClickListener {
                                override fun onItemClick(item: AllLawyerResponseItem) {

                                    getLawyerData(item)

                                }

                            })

                        recyclerView?.adapter = lawyerAdapter
                        for (i in list.distinctBy { it.full_name }) {
                            locationArrayList?.add(
                                LatLng(
                                    i.latitude.toDouble(),
                                    i.longitude.toDouble()
                                )
                            )
                            lat2 = locationArrayList?.get(0)?.latitude!!.toDouble()
                            long2 = locationArrayList?.get(0)?.longitude!!.toDouble()
                            lawyerArrayList?.add((i.full_name))
                            lawyerID?.add(i.id)

                        }

                        for (i in locationArrayList!!.indices) {
                            addLawyerMarker(LatLng(locationArrayList!![i]))
                        }
//                        addLawyerMarker(LatLng(lat2,long2))


                        lat1 = lat
                        long1 = longi
                    }


                }

                is Resource.Error -> {
                    hideProgressBar()
//                    checkPermissionGranted()
                    response.message?.let { message ->
                        rootLayout.errorSnack(message, Snackbar.LENGTH_LONG)

//                        lawyerAdapter.differ.submitList(response.data)

                    }

                }

                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }


    //    TODO
    @RequiresApi(Build.VERSION_CODES.M)
    private fun getLawyerData(item: AllLawyerResponseItem) {
        viewModel.getLawyerDetails(item.id)
        viewModel.lawyerPersonalDetails.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
//                    checkPermissionGranted()

                    response.data?.let {


//                        val simulateRoute = true
//                        val options = NavigationLauncherOptions.builder()
//                            .directionsRoute(currentRoute)
//                            .shouldSimulateRoute(simulateRoute)
//                            .build()
//
//                        // Call this method with Context from within an Activity
//                        NavigationLauncher.startNavigation(this, options)

//                        TODO
                        val facilaId = it.get(0).facial_id
                        val userId = it.get(0).id




                        Intent(
                            this@MapBoxActivity,
                            MainActivity::class.java
                        ).also {
                            it.putExtra(Constants.LAWYER_NAME, item.full_name)
                            it.putExtra(Constants.USER_ID, userId.toString())
//                            it.putExtra(Constants.USER_ID, facilaId.toString())
                            startActivity(it)
                        }
//



                    }
                }

                is Resource.Error -> {
                    hideProgressBar()
//                    checkPermissionGranted()
                    response.message?.let { message ->
                        rootLayout.errorSnack(message, Snackbar.LENGTH_LONG)

//                        lawyerAdapter.differ.submitList(response.data)

                    }

                }

                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

    }
    private fun hideProgressBar() {
        events_pb.visibility = View.GONE
    }

    private fun showProgressBar() {
        events_pb.visibility = View.VISIBLE
    }


    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }


    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        mapView.onSaveInstanceState(outState)
    }



    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val destinationPoint = Point.fromLngLat(marker.position.longitude,marker.position.latitude)

        val originPoint = Point.fromLngLat(locationComponent!!.lastKnownLocation!!.longitude, locationComponent!!.lastKnownLocation!!.latitude)
        Toast.makeText(this@MapBoxActivity, "Location update : $originPoint", Toast.LENGTH_LONG).show()

//        val source = mapboxMap!!.style!!.getSourceAs<GeoJsonSource>("destination-source-id")
//        source?.setGeoJson(Feature.fromGeometry(destinationPoint))
        getRoute(originPoint, destinationPoint)
        return true
    }


    override fun onBackPressed() {
        super.onBackPressed()
        Intent(
            this@MapBoxActivity,
            LoginActivity::class.java
        ).also {

            startActivity(it)
        }
    }

}