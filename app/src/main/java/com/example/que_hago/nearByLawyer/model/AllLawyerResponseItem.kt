package com.example.que_hago.nearByLawyer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllLawyerResponseItem(
    val id :Int,
    val full_name: String,
    val is_lawyer: Int,
    val latitude: Double,
    val longitude: Double,
    val distance:String,
    val isActive:Boolean
):Parcelable