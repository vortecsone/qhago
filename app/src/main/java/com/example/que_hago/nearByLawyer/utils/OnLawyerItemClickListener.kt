package com.example.que_hago.nearByLawyer.utils

import com.example.que_hago.nearByLawyer.model.AllLawyerResponseItem

interface OnLawyerItemClickListener {
    fun onItemClick(item: AllLawyerResponseItem)
}