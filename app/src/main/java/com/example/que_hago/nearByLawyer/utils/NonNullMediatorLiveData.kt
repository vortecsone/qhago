package com.example.que_hago.nearByLawyer.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/*
      * Created by Chetu..
  */

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()

fun <T> LiveData<T>.nonNull(): NonNullMediatorLiveData<T> {
    val mediator: NonNullMediatorLiveData<T> = NonNullMediatorLiveData()
    mediator.addSource(this) { it?.let { mediator.value = it } }
    return mediator
}