package com.example.mapmodule.nearbyLawer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList
import java.util.HashMap

/*
      * Created by Chetu..
  */

@Parcelize
data class PolylineData(val routeInfo : RouteInfoData,
                        val ployLineRoutesList : ArrayList<HashMap<String, String>>?) : Parcelable
