package com.example.mapmodule.nearbyLawer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/*
      * Created by Chetu..
  */


@Parcelize
data class RouteInfoData(val distance : String, val duration:String) : Parcelable