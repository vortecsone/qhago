package com.example.mapmodule.nearbyLawer.model

import androidx.annotation.DrawableRes

/*
      * Created by Chetu..
  */

data class MarkerData(val latitutde : Double, val longitude : Double, val title : String, val snippets: String, @DrawableRes val iconResID: Int)
