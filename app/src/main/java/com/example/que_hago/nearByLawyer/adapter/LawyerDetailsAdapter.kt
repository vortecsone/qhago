package com.example.que_hago.nearByLawyer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.que_hago.R
import com.example.que_hago.nearByLawyer.model.AllLawyerResponseItem
import com.example.que_hago.nearByLawyer.utils.OnLawyerItemClickListener

class LawyerDetailsAdapter(
    private val mList: List<AllLawyerResponseItem>,
    val listener: OnLawyerItemClickListener

) : RecyclerView.Adapter<LawyerDetailsAdapter.ViewHolder>() {


    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class
//        holder.imageView.setImageResource(ItemsViewModel.image)

        // sets the text to the textview from our itemHolder class
        holder.textView.text = ItemsViewModel.full_name.replaceFirstChar { it.uppercase() }
        holder.itemView.setOnClickListener { listener.onItemClick(ItemsViewModel) }

        holder.activeImg.setImageResource(R.drawable.ic_circle_green)
//      for Active lawyers..

      /*  if (ItemsViewModel.is_lawyer.equals(true)){
            holder.activeImg.setImageResource(R.drawable.ic_circle_green)
        }else{




            holder.activeImg.setImageResource(R.drawable.ic_circle_grey)
        }*/

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {

        return mList.size

    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.textView)
        val distanceTv: TextView = itemView.findViewById(R.id.distance)
//        val tvDuration: TextView = itemView.findViewById(R.id.tvDuration)
        val activeImg: ImageView = itemView.findViewById(R.id.lawyer_profile_image_view)

    }
}