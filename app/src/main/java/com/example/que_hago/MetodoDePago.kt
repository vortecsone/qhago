package com.example.que_hago

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar

class MetodoDePago(credit_card_number: String, card_type: String, is_selected: Boolean,
                   contentLayoutId: Int
) : Fragment(contentLayoutId) {

    private var credit_card_number: String? = null
    private var card_type: String? = null
    private var is_selected: Boolean? = null

    init {
        this.credit_card_number = credit_card_number
        this.card_type = card_type
        this.is_selected = is_selected
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_metodo_de_pago, container, false)
    }

}