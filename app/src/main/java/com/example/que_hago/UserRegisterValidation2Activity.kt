package com.example.que_hago

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.appbar.CollapsingToolbarLayout
import kotlinx.android.synthetic.main.activity_user_register_validation_2_content_scrolling.*
import org.apache.commons.io.IOUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*

class UserRegisterValidation2Activity : AppCompatActivity()
{
    private lateinit var tvResponseData: TextView
    var currentPhotoPath: String? = null
    var userID: Int? = 0
    var userRut: String? = null
    var userName: String? = null
    var userLastname: String? = null
    var imageb64: String? = ""
    var profileID = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_register_validation_2)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        initUIControls()
        getExtras()

        btnAddFacialPhoto.setOnClickListener {
            addFacialPhoto()
        }

        btnFacialRegister.setOnClickListener {
            facialRegister()
        }

        btnSkipFace.setOnClickListener {
            val intent = Intent(this, RolChoosingActivity::class.java)
            intent.putExtra("userID", userID!!)
            startActivity(intent)
        }
    }

    private fun facialRegister()
    {
        if (profileID.isEmpty())
        {
            try
            {
                tvResponseData.text = "CREANDO PERFIL: $userName $userLastname"
                PostAddProfile(userName!!,userLastname!!,userRut!!)
                Thread.sleep(5000)
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
        else if (imageb64!!.isEmpty())
            tvResponseData.text = "SUBA UNA IMAGEN DE SU ROSTRO Y VUELVA A INTENTAR"
        else
        {
            try
            {
                tvResponseData.text = "ASIGNANDO IMAGEN A USUARIO: $userName $userLastname"
                PostAddImageToProfile()
                Thread.sleep(5000)
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun addFacialPhoto()
    {
        try
        {
            dispatchTakePictureIntent()
            Thread.sleep(10000)
            galleryAddPic()
            imageb64 = bitmapToBase64(setPic())
        }
        catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private fun initUIControls()
    {
        tvResponseData = findViewById(R.id.tvResponseData)
        btnAddFacialPhoto.isEnabled = true
        btnFacialRegister.isEnabled = false
    }

    private fun getExtras()
    {
        userID = intent.getIntExtra("userID",0)
        userRut = intent.getStringExtra("userRut")
        userName = intent.getStringExtra("userName")
        userLastname = intent.getStringExtra("userLastname")
    }

    private fun dispatchTakePictureIntent()
    {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(packageManager) != null)
        {
            // Create the File where the photo should go
            var photoFile: File? = null
            try
            {
                photoFile = createImageFile()
            }
            catch (ex: IOException) { }
            // Continue only if the File was successfully created
            if (photoFile != null)
            {
                val photoURI = FileProvider.getUriForFile(this, "com.example.android.gnprovider", photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun galleryAddPic()
    {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(currentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)
    }

    private fun bitmapToBase64(bitmap: Bitmap): String
    {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP)
        writeToFile(encoded, this)
        btnAddFacialPhoto.text = "CAPTURAR OTRA IMAGEN"
        btnFacialRegister.isEnabled = true
        return encoded
    }

    private fun setPic(): Bitmap
    {
        // Get the dimensions of the View
        val targetW = ivFace!!.width
        val targetH = ivFace!!.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH))

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true
        val bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        ivFace!!.setImageBitmap(bitmap)
        return bitmap
    }

    @Throws(IOException::class)
    private fun createImageFile(): File
    {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.getExternalStorageDirectory().absolutePath + "/Pictures/")
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    private fun writeToFile(data: String, context: Context)
    {
        try
        {
            val outputStreamWriter = OutputStreamWriter(context.openFileOutput("test.txt", MODE_PRIVATE))
            outputStreamWriter.write(data)
            outputStreamWriter.close()
        }
        catch (e: IOException) {
            Log.e("Exception", "File write failed: $e")
        }
    }

    private fun readFromFile(context: Context): String
    {
        var ret = ""
        try
        {
            val inputStream: InputStream? = context.openFileInput("test.txt")
            if (inputStream != null)
            {
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                var receiveString: String? = ""
                val stringBuilder = StringBuilder()
                while (bufferedReader.readLine().also { receiveString = it } != null)
                {
                    stringBuilder.append(receiveString)
                }
                inputStream.close()
                ret = stringBuilder.toString()
            }
        }
        catch (e: FileNotFoundException) {
            Log.e("login activity", "File not found: $e")
        }
        catch (e: IOException) {
            Log.e("login activity", "Can not read file: $e")
        }
        return ret
    }

    companion object
    {
        const val REQUEST_IMAGE_CAPTURE = 1

        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Throws(Exception::class)
    private fun PostAddProfile(n: String, ln: String, id: String)
    {
        val requestQueue = Volley.newRequestQueue(this@UserRegisterValidation2Activity)
        //http://50.192.137.252:8080/iklab/ikface/api/profile/add?appKey=JDJiJDEwJGhwZC5ma3k5bnZFT3NIY0ZIUVJubC5nQlMvSUMyV0RBcTREYTRsV0lkOEpULzFMa0JIVVlPOmFkbWluOklLTEFCMDA0
        //val url = "http://8a2a0877d2e9.sn.mynetname.net:8081/iklab/ikface/api/profile/add"
        val url = "http://50.192.137.252:8080/iklab/ikface/api/profile/add?appKey=JDJiJDEwJGhwZC5ma3k5bnZFT3NIY0ZIUVJubC5nQlMvSUMyV0RBcTREYTRsV0lkOEpULzFMa0JIVVlPOmFkbWluOklLTEFCMDA0"
        //val atoken = IOUtils.toString(this.resources.openRawResource(R.raw.token), StandardCharsets.UTF_8)
        //val json = "{ \"atoken\":\"$atoken\", \"firstname\":\"$n\", \"lastname\":\"$ln\", \"personalId\":\"$id\", \"personalIdType\":\"RUT\" , \"categories\":\"[4]\"  }"
        val json = "{ \"firstname\":\"$n\", \"lastname\":\"$ln\", \"personalId\":\"$id\", \"personalIdType\":\"RUT\" , \"categories\":\"[1]\"  }"
        val jsonBody = JSONObject(json)
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(url, jsonBody, Response.Listener { response ->
            try
            {
                val cod = response.getString("cod")
                if (cod == "200") {
                    profileID = response.getString("profileId")
                    tvResponseData!!.text = "CREANDO USUARIO...   $userName $userLastname"
                    Thread.sleep(3000)
                    tvResponseData!!.text = "ID PERFIL : $profileID"
                    btnFacialRegister.text = "TERMINAR REGISTRO FACIAL"
                    Thread.sleep(5000)
                }
                else
                {
                    val mssg = response.getString("mssg")
                    tvResponseData!!.text = "$cod: $mssg"
                }
            }
            catch (e: JSONException) {
                e.printStackTrace()
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { tvResponseData!!.text = "Post Data : Response Failed" })
        {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>
            {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json"
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Throws(Exception::class)
    private fun PostAddImageToProfile()
    {
        val requestQueue = Volley.newRequestQueue(this@UserRegisterValidation2Activity)
        //val url = "http://8a2a0877d2e9.sn.mynetname.net:8081/iklab/ikface/api/image/add"
        val url = "http://50.192.137.252:8080/iklab/ikface/api/image/add?appKey=JDJiJDEwJGhwZC5ma3k5bnZFT3NIY0ZIUVJubC5nQlMvSUMyV0RBcTREYTRsV0lkOEpULzFMa0JIVVlPOmFkbWluOklLTEFCMDA0"
        //val atoken = IOUtils.toString(this.resources.openRawResource(R.raw.token), StandardCharsets.UTF_8)
        val imageb64 = readFromFile(this)
        //val json = "{ \"atoken\":\"$atoken\", \"profileId\":\"$profileID\", \"ext\":\"JPEG\", \"imageB64\":\"$imageb64\", \"imageAvatar\":\"false\"   }"
        val json = "{ \"profileId\":\"$profileID\", \"ext\":\"JPEG\", \"imageB64\":\"$imageb64\", \"imageAvatar\":\"false\"   }"
        val jsonBody = JSONObject(json)
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(url, jsonBody, Response.Listener { response ->
            try
            {
                val cod = response.getString("cod")
                if (cod == "200")
                {
                    tvResponseData!!.text = "IMAGEN ASOCIADA AL ID  DE PERFIL : $profileID"
                    Thread.sleep(3000)
                    tvResponseData!!.text = "CONSTRUYENDO PERFIL...   $userName $userLastname"
                    GetBuildProfile(profileID)
                    Thread.sleep(5000)
                }
                else
                {
                    val mssg = response.getString("mssg")
                    tvResponseData!!.text = "$cod: $mssg"
                }
            }
            catch (e: JSONException) {
                e.printStackTrace()
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
        },
            Response.ErrorListener { tvResponseData!!.text = "Post Data : Response Failed" })
        {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>
            {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json"
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Throws(IOException::class)
    private fun GetBuildProfile(id: String)
    {
        val queue = Volley.newRequestQueue(this@UserRegisterValidation2Activity)
        //val atoken = IOUtils.toString(this.resources.openRawResource(R.raw.token), StandardCharsets.UTF_8)
        //val url = "http://8a2a0877d2e9.sn.mynetname.net:8081/iklab/ikface/api/profile/buildfrp?atoken=$atoken&profileId=$id"
        val url = "http://50.192.137.252:8080/iklab/ikface/api/profile/buildfrp?appKey=JDJiJDEwJGhwZC5ma3k5bnZFT3NIY0ZIUVJubC5nQlMvSUMyV0RBcTREYTRsV0lkOEpULzFMa0JIVVlPOmFkbWluOklLTEFCMDA0&profileId=2"

        val stringRequest = StringRequest(Request.Method.GET, url, { response ->
            try
            {
                tvResponseData!!.text = "Construyendo perfil de reconocimiento facial..."
                Thread.sleep(3000)
                val jsonObject = JSONObject(response)
                if (jsonObject.getString("cod") == "200") tvResponseData!!.text = "PERFIL DE RECONOCIMIENTO FACIAL CREADO CORRECTAMENTE" else tvResponseData!!.text = "ERROR! COD :" + jsonObject.getString("cod") + " INTENTE OTRA VEZ"
            }
            catch (e: Exception) {
                e.printStackTrace()
                tvResponseData!!.text = "ERROR EN CONVERSION A FORMATO JSON"
            }
        }
        ) { tvResponseData!!.text = "ERROR AL CONSTRUIR PERFIL" }
        queue.add(stringRequest)
    }


}
