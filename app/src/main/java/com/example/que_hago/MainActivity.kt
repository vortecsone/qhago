package com.example.que_hago

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.que_hago.localDB.SharedPreference
import com.example.que_hago.moveToMapModule.DisplayMapModule
import com.example.que_hago.moveToTraceModule.DisplayTraceModule
import com.example.que_hago.nearByLawyer.utils.Constants
import com.example.que_hago.nearByLawyer.view.MapBoxActivity
import com.example.que_hago.nearByLawyer.view.NearBylawyerInMapActivity
import com.example.videomodule.VideoCallActivity
//import com.google.android.gms.maps.model.LatLng
//import com.mesibo.api.Mesibo
//import com.mesibo.api.Mesibo.getSelfProfile
//import com.mesibo.api.MesiboProfile
//import com.mesibo.calls.api.MesiboCall
//import com.mesibo.calls.api.MesiboCallActivity
//import com.mesibo.messaging.MesiboUI
import kotlinx.android.synthetic.main.price_dialog.view.*
import org.apache.commons.io.IOUtils
import org.json.JSONObject
import java.math.RoundingMode
import java.nio.charset.StandardCharsets
import java.text.DecimalFormat


class MainActivity : AppCompatActivity() {
    internal inner class DemoUser(var token: String, var name: String, var address: String)

    private var mRemoteUser: DemoUser? = null
//    private var mProfile: MesiboProfile? = null
//    var mReadSession: Mesibo.ReadDbSession? = null

    //var mLoginButton1: View? = null
    //var mLoginButton2: View? = null
    //var mSendButton: Button? = null
    var mUiButton: View? = null
    var mAudioCallButton: View? = null
    var mVideoCallButton: View? = null
    var userName: TextView? = null

    var mMessageStatus: TextView? = null


    var mConnStatus: TextView? = null
    var mMessage: EditText? = null

    //DEMO USERS AND THEIRS ACCESS TOKEN
    private var mUser1 = DemoUser(
        "f9bfb631d2db13a6258fccf0a4e1d64936d9daa2cb8f3f08ebb32b496",
        "Felipe Morales",
        "Lawyer1"
    )
    private var mUser2 = DemoUser(
        "c7583deaf4af8331ed645477f9f25dcfdb22376abde37ecd86e9932b497",
        "Cliente-1",
        "Client1"
    )


    private var userType: String = ""

    private var startTime: Long = 0
    private var currentTime: Long = 0
    private var duration: Long = 0


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //mLoginButton1 = findViewById(R.id.login1)
        //mLoginButton2 = findViewById(R.id.login2)
        //mSendButton = findViewById(R.id.send)
        mUiButton = findViewById(R.id.launchUI)
        mAudioCallButton = findViewById(R.id.audioCall)
        mVideoCallButton = findViewById(R.id.videoCall)
        userName = findViewById(R.id.tvLawyerName)


        //mMessageStatus = findViewById(R.id.msgStatus)
        //mConnStatus = findViewById(R.id.connStatus)
        mMessage = findViewById(R.id.message)

        //mSendButton?.setEnabled(false)
        mUiButton?.isEnabled = false
        mAudioCallButton?.isEnabled = false
        mVideoCallButton?.isEnabled = true
        mVideoCallButton?.setOnClickListener {
            val intent = Intent(this, VideoCallActivity::class.java)
            startActivity(intent)

        }

//        userType = intent.getStringExtra("userType")!!

        val i = intent
        val name = i.getStringExtra(Constants.LAWYER_NAME)
        val facialId = i.getStringExtra(Constants.USER_ID)

//        userName?.setText(""+name?.replaceFirstChar { it.uppercase() }
        userName?.setText("" + name?.replaceFirstChar { it.uppercase() })

        if (userType == "client")
//            mesiboInit(mUser1, mUser2)
        else
//            mesiboInit(mUser2, mUser1)

//        static map
//        val fromPosition = LatLng(28.704060, 77.102493)
//        DisplayMapModule(this).showMapStatic(this, fromPosition)

//      For Profile Image..

            getProfileImage(facialId)

    }

    private fun getProfileImage(facialId: String?) {

        val queue = Volley.newRequestQueue(this@MainActivity)
        val atoken =
            IOUtils.toString(this.resources.openRawResource(R.raw.token), StandardCharsets.UTF_8)
        val url =
            "http://8a2a0877d2e9.sn.mynetname.net:8081/iklab/ikface/api/profile/buildfrp?atoken=$atoken&profileId=$facialId"
        val stringRequest = StringRequest(Request.Method.GET, url, { response ->
            try {
//                    tvResponseData!!.text = "Construyendo perfil de reconocimiento facial..."
                Thread.sleep(3000)
                val jsonObject = JSONObject(response)
                if (jsonObject.getString("cod") == "200") {
//                        tvResponseData!!.text = "PERFIL DE RECONOCIMIENTO FACIAL CREADO CORRECTAMENTE"
                }
//
                else {
//                        tvResponseData!!.text = "ERROR! COD :" + jsonObject.getString("cod") + " INTENTE OTRA VEZ"
                }

            } catch (e: Exception) {
                e.printStackTrace()
//                    tvResponseData!!.text = "ERROR EN CONVERSION A FORMATO JSON"
            }
        }
        ) {
//                tvResponseData!!.text = "ERROR AL CONSTRUIR PERFIL"
        }
        queue.add(stringRequest)


    }


    fun onLoginUser1(view: View?) {
    }

    fun onLoginUser2(view: View?) {
    }

    fun onLaunchMessagingUi(view: View?) {
    }

    fun onAudioCall(view: View?) {

        pricingDialog()
//        PricingAlert()
//        //MesiboCall.getInstance().callUi(this, mProfile!!.address, false)
//        MesiboCall.getInstance().callUi(this, mRemoteUser!!.address, false)

//        if (!mCall!!.isLinkUp()) {
//            val intent = Intent(this, TracerActivity::class.java)
//            startActivity(intent)
//            return
//        }


    }

    private fun pricingDialog() {

        val mDialogView = LayoutInflater.from(this).inflate(R.layout.price_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView).setTitle("QHAGO ALERTA")
            .setMessage("EL VALOR DE LA LLAMADA TIENE UN COSTO DE x PESOS POR MINUTO, EL PRIMER MINUTO ES GRATIS. ¿ACEPTAR?")

        val mAlertDialog = mBuilder.show()
        //login button click of custom layout
        mDialogView.dialogLoginBtn.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
//            MesiboCall.getInstance().callUi(this, mRemoteUser!!.address, false)
        }
        //cancel button click of custom layout
        mDialogView.dialogCancelBtn.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
        }

    }


    fun onVideoCall(view: View?) {
        PricingAlert()

    }

    fun onTraceCall(view: View?) {
//            val intent = Intent(this, TracerActivity::class.java)
//            startActivity(intent)


//        Send Ip Address to Trace module..

        DisplayTraceModule(this).showTrace(this, "179.60.192.36", "216.58.201.238")
    }


//    override fun Mesibo_onFile(messageParams: Mesibo.MessageParams?, fileInfo: Mesibo.FileInfo?) {}


    fun PricingAlert() {
        //"LA TARIFA POR MINUTO QUE COBRAREMOS POR TI ES DE x PESOS POR MINUTO, ACORDE A TU VALOR DEFINIDO. SERA PAGADO EN DIAS HABILES"
        var message = ""
        if (userType == "lawyer")
            message =
                "LA GANANCIA POR LA LLAMADA TIENE UN VALOR DE x PESOS POR MINUTO, EL PRIMER MINUTO ES GRATIS"
        else
            message =
                "EL COSTO DE LA LLAMADA TIENE UN VALOR DE x PESOS POR MINUTO, EL PRIMER MINUTO ES GRATIS"
        val toast = Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_LONG
        )
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.show()
    }

    fun TotalPriceAlert() {
        //"LA TARIFA POR MINUTO QUE COBRAREMOS POR TI ES DE x PESOS POR MINUTO, ACORDE A TU VALOR DEFINIDO. SERA PAGADO EN DIAS HABILES"
        var message = ""
        if (userType == "lawyer")
            message = "DURACION LLAMADA: $duration"
        else
            message = "DURACION LLAMADA: $duration"
        val toast = Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_LONG
        )
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.show()
    }

    fun TimeAlert(m: String) {
        val message = m
        val toast = Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_LONG
        )
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.show()
    }

    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
        Toast.makeText(
            applicationContext,
            android.R.string.yes, Toast.LENGTH_SHORT
        ).show()
    }

    val negativeButtonClick = { dialog: DialogInterface, which: Int ->
        Toast.makeText(
            applicationContext,
            android.R.string.no, Toast.LENGTH_SHORT
        ).show()
    }

    fun basicAlert() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("QHAGO ALERTA")
            setMessage("EL VALOR DE LA LLAMADA TIENE UN COSTO DE x PESOS POR MINUTO, EL PRIMER MINUTO ES GRATIS. ¿ACEPTAR?")
            setPositiveButton("SI", DialogInterface.OnClickListener(function = positiveButtonClick))
            setNegativeButton("NO", DialogInterface.OnClickListener(function = negativeButtonClick))
            show()
        }
    }


    fun getCallPricing(secondPrice: Double, duration: Double): Int {
        var total = 0
        total = (secondPrice * duration).toInt()
        return total
    }

    fun roundOffDecimal(number: Double): Double? {
        val df = DecimalFormat("#.##    ")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        Intent(
            this@MainActivity,
            MapBoxActivity::class.java
        ).also {

            startActivity(it)
        }
    }
}