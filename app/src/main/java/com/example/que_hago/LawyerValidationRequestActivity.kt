package com.example.que_hago

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.lawyer_validation_request_content_scrolling.*
import retrofit2.Retrofit

class LawyerValidationRequestActivity : AppCompatActivity()
{
    var compositeDisposable = CompositeDisposable()
    lateinit var myAPI: INodeJS

    var userId: Int? = 0
    var titleCertificate = "X"
    var bank: String? = ""
    var accountType: String?  = ""
    var accountNumber: String? = ""
    var editHourPrice: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lawyer_validation_request)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        initDBApi()
        getExtras()
        initUIControls()

        /*btnSearchCertificate.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }*/

        btnLawyerValidationRequest.setOnClickListener {
            registerLawyer(userId!!, titleCertificate!!, bank!!, accountType!!, accountNumber!!, editHourPrice!!.text.toString().toInt() )
            val intent = Intent(this, LawyerListActivity::class.java)
            intent.putExtra("userType","lawyer")
            startActivity(intent)
        }

    }

    private fun initUIControls()
    {
        editHourPrice = findViewById(R.id.etHourPrice)
    }

    private fun registerLawyer(uID: Int, tC: String, b: String, aT: String, aN: String, hP: Int)
    {
        compositeDisposable.add(myAPI.registerLawyer(uID, tC, b, aT, aN, hP)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                if (message.equals("\"REGISTER SUCESSFUL\""))
                    Toast.makeText(this@LawyerValidationRequestActivity ,"REGISTRO CORRECTO COMO ABOGADO", Toast.LENGTH_LONG).show()
                else
                    Toast.makeText(this@LawyerValidationRequestActivity,message, Toast.LENGTH_LONG).show()
            })
    }

    private fun initDBApi()
    {
        val retrofit : Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)
    }

    private fun getExtras()
    {
        userId = intent.getIntExtra("userID",0)
        bank = intent.getStringExtra("bank")
        accountType = intent.getStringExtra("accountType")
        accountNumber = intent.getStringExtra("accountNumber")
    }

    override fun onStop()
    {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy()
    {
        compositeDisposable.clear()
        super.onDestroy()
    }




}