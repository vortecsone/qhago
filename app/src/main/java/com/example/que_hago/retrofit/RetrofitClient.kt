package com.example.que_hago.retrofit

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


object RetrofitClient
{
    private var ourInstace:Retrofit ?= null

    val instance:Retrofit
        get() {

            if (ourInstace == null)
                ourInstace = Retrofit.Builder()
                    .baseUrl("http://50.192.137.250:3000/") // http://10.0.2.2:3000/ LOCALHOST ON EMULATOR http://45.225.92.140:3000/
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build()
            return ourInstace!!

        }

}

// client base url : http://45.225.92.140:3000/"
// chetu base url : http://172.16.43.15:3000/"