package com.example.que_hago.retrofit

import  io.reactivex.Observable
import  retrofit2.http.Field
import  retrofit2.http.FormUrlEncoded
import  retrofit2.http.POST
import java.security.cert.Certificate

interface INodeJS {
    @POST("register")
    @FormUrlEncoded
    fun registerUser(
        @Field("rut") rut: String,
        @Field("email") email: String,
        @Field("name") name: String,
        @Field("lastname") lastname: String,
        @Field("password") password: String
    ): Observable<String>

    @POST("registerLawyer")
    @FormUrlEncoded
    fun registerLawyer(
        @Field("userId") userId: Int,
        @Field("titleCertificate") titleCertificate: String,
        @Field("bank") bank: String,
        @Field("accountType") accountType: String,
        @Field("accountNumber") accountNumber: String,
        @Field("hourPrice") hourPrice: Int
    ): Observable<String>

    @POST("login")
    @FormUrlEncoded
    fun loginUser(
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<String>

    @POST("getLawyerAll")
    @FormUrlEncoded
    fun getLawyerAll(@Field("lawyerName") lawyerName: String): Observable<String>

    @POST("getLawyerByName")
    @FormUrlEncoded
    fun getLawyerByName(@Field("lawyerName") lawyerName: String): Observable<String>


    //FUNCTIONS FOR MANAGE USER LOCATION IN REAL TIME
    @POST("getLocationByUser")
    @FormUrlEncoded
    fun getLocationByUser(@Field("userID") userID: Int): Observable<String>

    @POST("registerUserLocation")
    @FormUrlEncoded
    fun registerUserLocation(
        @Field("userID") userId: Int,
        @Field("longitude") longitude: Double,
        @Field("latitude") latitude: Double
    ): Observable<String>

    @POST("getLawyerDataByID")
    @FormUrlEncoded
    fun getLawyerDataByID(@Field("lawyerID") lawyerId: Int): Observable<String>








}