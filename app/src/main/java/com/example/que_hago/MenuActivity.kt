package com.example.que_hago

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar


class   MenuActivity : AppCompatActivity()
{

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var arraylist:ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        //GET USER TYPE AFTER LOGIN
        var userType = intent.getStringExtra("userType")

        val chat: FloatingActionButton = findViewById(R.id.chat)
        chat.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            val chatView = Intent(this@MenuActivity, MainActivity::class.java)
            //SEND USER TYPE TO CHAT VIEWS
            if (userType=="lawyer")

                chatView.putExtra("userType", "lawyer")
            else
                chatView.putExtra("userType", "client")
            //
            startActivity(chatView)
        }

        val camera: FloatingActionButton = findViewById(R.id.camera)
        camera.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            val cameraView = Intent(this@MenuActivity, FacialMatchActivity::class.java)
            startActivity(cameraView)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.view_fragment)


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.menu_home,
                R.id.menu_editar_perfil,
                R.id.menu_mensajes,
                R.id.menu_servicios,
                R.id.menu_historial_de_busqueda,
                R.id.menu_favoritos,
                R.id.menu_faq,
                R.id.menu_identificacion,
                R.id.menu_metodos_de_pago

            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        //onFragmentMenuHome()
    }
    private fun launchChat()
    {

    }

    private fun onFragmentMenuHome()
    {
        //creación de variables para la searchview de fragment_menu_home. La busqueda se realiza a partir de este segmento
        /*
        val search = findViewById<SearchView>(R.id.buscador)
        val names = arrayOf("Android", "Java", "Php", "Python", "C++")
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            this, android.R.layout.simple_list_item_1, names
        )
         */
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean
    {
        val navController = findNavController(R.id.view_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


}