package com.example.que_hago

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.stripemodule.StoreActivity
import kotlinx.android.synthetic.main.activity_call_price.*
import kotlin.math.roundToInt
import kotlin.math.truncate

class CallPriceActivity : AppCompatActivity()
{
    private lateinit var tvHourPrice: TextView
    private lateinit var tvMinutePrice: TextView
    private lateinit var tvSecondPrice: TextView
    private lateinit var tvDuration: TextView
    private lateinit var tvTotalPrice: TextView

    private var hourPrice: Int = 0
    private var minutePrice: Double = 0.0
    private var secondPrice: Double = 0.0
    private var duration: Double = 0.0
    private var totalPrice: Int = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_price)

        hourPrice = intent.getIntExtra("hourPrice",0)
        minutePrice = intent.getDoubleExtra("minutePrice",0.0)
        secondPrice = intent.getDoubleExtra("secondPrice",0.0)
        duration = intent.getDoubleExtra("duration",0.0)
        totalPrice = intent.getIntExtra("totalPrice",0)

        tvHourPrice = findViewById(R.id.tvHourPrice)
        tvMinutePrice = findViewById(R.id.tvMinutePrice)
        tvSecondPrice = findViewById(R.id.tvSecondPrice)
        tvDuration = findViewById(R.id.tvDuration)
        tvTotalPrice = findViewById(R.id.tvTotalPrice)

        tvHourPrice.text = "VALOR POR HORA $: $hourPrice"
        tvMinutePrice.text = "VALOR POR MINUTO $: $minutePrice"
        tvSecondPrice.text = "VALOR POR SEGUNDO $: $secondPrice"
        tvDuration.text = "DURACION LLAMADA: $duration"
        tvTotalPrice.text = "VALOR TOTAL LLAMADA $: $totalPrice"

        btnAccept.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, StoreActivity::class.java)
            //intent.putExtra("userType","lawyer")
            intent.putExtra("callDuration", duration.toInt())
            startActivity(intent)
        })


    }


}