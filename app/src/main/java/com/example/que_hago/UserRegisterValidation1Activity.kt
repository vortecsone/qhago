package com.example.que_hago

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.appbar.CollapsingToolbarLayout
import kotlinx.android.synthetic.main.activity_user_register_validation_1_content_scrolling.*

class UserRegisterValidation1Activity : AppCompatActivity()
{
    var userID: Int? = 0
    var userRut: String = ""
    var userName: String = ""
    var userLastname: String = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_register_validation_1)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        initUIControls()
        getExtras()

        btnAddPhotoIDCard.setOnClickListener {
            val intent = Intent(this, UserRegisterValidation2Activity::class.java)
            startActivity(intent)
        }

        btnIDCardRegister.setOnClickListener {
            val intent = Intent(this, UserRegisterValidation2Activity::class.java)
            intent.putExtra("userID", userID)
            intent.putExtra("userRut", userRut)
            intent.putExtra("userName", userName)
            intent.putExtra("userLastname", userLastname)
            startActivity(intent)
        }

        /*btnSkipFace.setOnClickListener {
            val intent = Intent(this, UserRegisterActivity::class.java)
            startActivity(intent)
        }*/
    }

    private fun initUIControls()
    {
        btnAddPhotoIDCard.isEnabled=true
    }

    private fun getExtras()
    {
        userID = intent.getIntExtra("userID",0)
        userRut = intent.getStringExtra("userRut").toString()
        userName = intent.getStringExtra("userName").toString()
        userLastname = intent.getStringExtra("userLastname").toString()
    }


}