package com.example.que_hago.moveToTraceModule

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.tracermodule.view.TracerActivity
//import org.webrtc.NetworkMonitorAutoDetect

class DisplayTraceModule (val context: Context){

    fun showTrace(context: Context, fromIpAddress: String,toIpAddress: String) {

        val intent = Intent( context, TracerActivity::class.java)
        val  fromIpAdd=fromIpAddress
        val toIPAdd=toIpAddress
        intent.putExtra("from_ipAddress", fromIpAdd)
        intent.putExtra("to_ipAddress", toIPAdd)
        (context as Activity).startActivity(intent)
    }
}


