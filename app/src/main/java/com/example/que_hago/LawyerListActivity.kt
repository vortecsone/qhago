package com.example.que_hago

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.que_hago.databinding.ActivityLawyerListBinding
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import retrofit2.Retrofit

class LawyerListActivity : AppCompatActivity()
{
    lateinit var myAPI: INodeJS
    var compositeDisposable = CompositeDisposable()
    private var messageJson: JSONArray = JSONArray()

    private lateinit var binding: ActivityLawyerListBinding
    private lateinit var adapter: RVAdapter
    private lateinit var sView: SearchView

    private lateinit var list: ArrayList<Content>

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        binding = ActivityLawyerListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //INIT API
        val retrofit : Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)

        list = ArrayList<Content>()
        getLawyerAll("")
        adapter = RVAdapter(this, list)

        sView = findViewById(R.id.buscador)
        sView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

    }
//
//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
//
//        //val item = menu?.findItem(R.id.searchView_MenuMain)
//
//        /*val searchView: SearchView = item?.actionView as SearchView
//        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
//        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return false
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//                adapter.filter.filter(newText)
//                return false
//            }
//        })*/
//
//        return true
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return true
    }

    private fun addList(t: String, d: String)
    {
        if (d=="Penal")
            list.add(Content(t, d, R.mipmap.ic_abogado_test))
        else
            list.add(Content(t, d, R.mipmap.ic_qhago))
    }

    private fun getLawyerAll(lawyerName: String)
    {
        compositeDisposable.add(myAPI.getLawyerAll(lawyerName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                var formatted = message.replace("\"", "'")
                messageJson = JSONArray(formatted)
                for (i in 0 until messageJson.length())
                {
                    val item = messageJson.getJSONObject(i)
                    val title = item.getString("price_name")
                    //addToList(title, "No definido", R.mipmap.ic_qhago)
                    when (i) {
                        0 -> addList("S.O.S", "Servicio de emergencia")
                        1 -> addList("Mi Testigo", "Servicio de testigo")
                        2 -> addList(title, "Penal")
                        else -> addList(title, "Contencioso")
                    }
                    adapter = RVAdapter(this, list)
                    binding.recyclerView.layoutManager = LinearLayoutManager(this)
                    binding.recyclerView.adapter = adapter
                }

            })
    }





}