package com.example.que_hago

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.lawyer_bank_account_content_scrolling.*

class LawyerBankAccountActivity : AppCompatActivity()
{
    var compositeDisposable = CompositeDisposable()
    var editBank: EditText? = null
    var editAccountType: EditText? = null
    var editAccountNumber: EditText? = null

    var userID: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lawyer_bank_account)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        editBank = findViewById(R.id.etBank)
        editAccountType = findViewById(R.id.etAccountType)
        editAccountNumber = findViewById(R.id.etAccountNumber)

        userID = intent.getIntExtra("userID",0)

        btnLawyerBankAccount.setOnClickListener {
            val intent = Intent(this, LawyerValidationRequestActivity::class.java)
            intent.putExtra("userID",userID!!)
            intent.putExtra("bank", editBank!!.text.toString() )
            intent.putExtra("accountType",editAccountType!!.text.toString())
            intent.putExtra("accountNumber",editAccountNumber!!.text.toString())
            startActivity(intent)
        }


        /*btnAsLawyer.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }*/
    }

    override fun onStop()
    {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy()
    {
        compositeDisposable.clear()
        super.onDestroy()
    }




}