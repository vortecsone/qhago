package com.example.que_hago.ui.home

import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.que_hago.Content
import com.example.que_hago.R
import com.example.que_hago.RVAdapter
import com.example.que_hago.databinding.ActivityLawyerListBinding
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import retrofit2.Retrofit

class HomeFragment : Fragment()
{
    lateinit var myAPI: INodeJS
    var compositeDisposable = CompositeDisposable()
    private var messageJson: JSONArray = JSONArray()

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: ActivityLawyerListBinding
    private lateinit var adapter: RVAdapter
    private lateinit var sView: SearchView
    private lateinit var list: ArrayList<Content>

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View?
    {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        binding = ActivityLawyerListBinding.inflate(layoutInflater)
        val root = inflater.inflate(R.layout.fragment_menu_home, container, false)

        //INIT API
        val retrofit : Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)

        list = ArrayList<Content>()
        getLawyerAll("")
        adapter = RVAdapter(this.requireActivity(), list)

        sView = root.findViewById(R.id.buscador)
        sView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

        //return binding.root
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return true
    }

    private fun addList(t: String, d: String)
    {
        if (d=="Servicio de emergencia")
            list.add(Content(t, d, R.mipmap.ic_sos))
        else if (d=="Servicio de testigo")
            list.add(Content(t, d, R.mipmap.ic_qhago))
        else if (d=="Penal")
            list.add(Content(t, d, R.mipmap.ic_abogado_test))

    }

    private fun getLawyerAll(lawyerName: String)
    {
        compositeDisposable.add(myAPI.getLawyerAll(lawyerName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                var formatted = message.replace("\"", "'")
                messageJson = JSONArray(formatted)
                for (i in 0 until messageJson.length())
                {
                    val item = messageJson.getJSONObject(i)
                    val title = item.getString("price_name")
                    //addToList(title, "No definido", R.mipmap.ic_qhago)
                    when (i) {
                        0 -> addList("S.O.S", "Servicio de emergencia")
                        1 -> addList("Mi Testigo", "Servicio de testigo")
                        2 -> addList(title, "Penal")
                        else -> addList(title, "Contencioso")
                    }
                    adapter = RVAdapter(this.requireActivity(), list)
                    binding.recyclerView.layoutManager = LinearLayoutManager(this.requireActivity())
                    binding.recyclerView.adapter = adapter
                }

            })
    }







}