package com.example.que_hago.ui.identification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class IdentificationViewModel : ViewModel()
{

    private val _text = MutableLiveData<String>().apply {
        value = "This is identification Fragment"
    }
    val text: LiveData<String> = _text
}