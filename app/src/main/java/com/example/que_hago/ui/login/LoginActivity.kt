package com.example.que_hago.ui.login

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.que_hago.*
import com.example.que_hago.databinding.ActivityLoginBinding
import com.example.que_hago.localDB.SharedPreference
import com.example.que_hago.nearByLawyer.utils.*
import com.example.que_hago.nearByLawyer.view.MapBoxActivity
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import com.example.videomodule.TextToSpeechActivity
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mayowa.android.locationwithlivedata.LocationViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit


class LoginActivity : AppCompatActivity() {
    lateinit var loginBinding: ActivityLoginBinding

    private lateinit var callbackManager: CallbackManager
    lateinit var myAPI: INodeJS
    var compositeDisposable = CompositeDisposable()
    var editEmail: EditText? = null
    var editPassword: EditText? = null
    private var permissionHelper: PermissionHelper? = null

    var id = 0
    private var uiHelper: UiHelper? = null
    private var isPermissionPermanentlyDenied = false
    private var gpsSetting: GpsSetting? = null

    lateinit var sharedPreference: SharedPreference
    lateinit var locationTrack: LocationTrack
    private lateinit var locationViewModel: LocationViewModel
    private var latlng: LatLng = LatLng(0.0, 0.0)


    private val LOCATION_PERMISSION_REQ_CODE = 1000;
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)





//        getCurrentLocation()

        callbackManager = CallbackManager.Factory.create();
        //loginBinding.loginButton.setReadPermissions(listOf("email","public_profile","user_gender","user_birthday","user_friends"))
        loginBinding.loginButton.setReadPermissions(listOf("email", "public_profile"))
        /*loginBinding.loginButton.registerCallback(callbackManager, object :FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?)
            {
            }
            override fun onCancel()
            {
            }
            override fun onError(error: FacebookException?)
            {
            }
        })*/

        editEmail = findViewById(R.id.edt_email)
        editPassword = findViewById(R.id.edt_password)
        val login = findViewById<Button>(R.id.login)
        val no_login = findViewById<Button>(R.id.no_login)
        val register = findViewById<Button>(R.id.register)

        sharedPreference = SharedPreference(this)

        id = sharedPreference.getValueInt("userID")

        login.isEnabled = true

        //INIT API
        val retrofit: Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)


        setInit()
//        locationPermission()

//        locationTrack = LocationTrack(this)


//        if (locationTrack.canGetLocation) {
//            updateLocation()
//        }


        login.setOnClickListener {
//            login(editEmail!!.text.toString(), editPassword!!.text.toString())

            val intent = Intent(this, MapBoxActivity::class.java)
            startActivity(intent)

        }

        no_login.setOnClickListener {
            val intent = Intent(this, TextToSpeechActivity::class.java)
//            val intent = Intent(this, MainActivity::class.java)
//            val intent = Intent(this, MenuActivity::class.java)
//            val intent = Intent(this, VideoCallActivity::class.java)
            //val intent = Intent(this, FacialMatchActivity::class.java)
            //val intent = Intent(this, LawyerListActivity::class.java)
            intent.putExtra("userType", "client")
            startActivity(intent)

        }

        register.setOnClickListener {
            //val intent = Intent(this, UserRegisterValidation2Activity::class.java)
            val intent = Intent(this, UserRegisterActivity::class.java)
            startActivity(intent)

//            val intent = Intent(this, MapsActivity::class.java)
//            startActivity(intent)
        }


        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


//
//
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                )
//            ) {
//
//            } else {
//
//            }
//            return
//        }

        val locationPermissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                when {
                    permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {

                        // Precise location access granted.

                        fusedLocationClient.lastLocation
                            .addOnSuccessListener { location ->
                                // getting the last known or current location
                                if (location != null) {
                                    latitude = location.latitude
                                    longitude = location.longitude
                                    Toast.makeText(
                                        this,
                                        "Latitude " + latitude + "\nLongitude " + longitude,
                                        Toast.LENGTH_LONG
                                    ).show()
                                    Log.d("deb", "location: " + latitude)
                                }


                            }
                            .addOnFailureListener {
                                Toast.makeText(
                                    this, "Failed on getting current location",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                    }
                    permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                        // Only approximate location access granted.

//                        if (ActivityCompat.checkSelfPermission(
//                                this,
//                                Manifest.permission.ACCESS_FINE_LOCATION
//                            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                                this,
//                                Manifest.permission.ACCESS_COARSE_LOCATION
//                            ) != PackageManager.PERMISSION_GRANTED
//                        ) {
//                            // TODO: Consider calling
//                            //    ActivityCompat#requestPermissions
//                            // here to request the missing permissions, and then overriding
//                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                            //                                          int[] grantResults)
//                            // to handle the case where the user grants the permission. See the documentation
//                            // for ActivityCompat#requestPermissions for more details.
//                            return@registerForActivityResult
//                        }
                        fusedLocationClient.lastLocation
                            .addOnSuccessListener { location ->
                                // getting the last known or current location
                                if (location != null) {
                                    latitude = location.latitude
                                    longitude = location.longitude
                                    Toast.makeText(
                                        this,
                                        "Latitude " + latitude + "\nLongitude " + longitude,
                                        Toast.LENGTH_LONG
                                    ).show()

                                }


                            }
                            .addOnFailureListener {
                                Toast.makeText(
                                    this, "Failed on getting current location",
                                    Toast.LENGTH_SHORT
                                ).show()


                    }
                    }
                    else -> {
                        // No location access granted.

                    }
                }
            }
        }
        locationPermissionRequest.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )


    }

    private fun getCurrentLocation() {
        // checking location permission
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // request permission
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQ_CODE
            );
            return
        }

    }

    private fun setInit() {
        uiHelper = UiHelper(this)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        permissionHelper = PermissionHelper(this, uiHelper!!)

    }


/*
     * Checking whether Location Permission is granted or not.
     * */

//    @RequiresApi(Build.VERSION_CODES.M)
//    private fun checkPermissionGranted() {
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) !== PackageManager.PERMISSION_GRANTED
//        ) permissionHelper?.openSettingsDialog()
//        else {
//            enableGps()
//        }
//    }

    /**
     * This function is to get the result form [PermissionHelper] class
     *
     * @param isPermissionGranted the [Boolean]
     */


//    private fun enableGps() {
//        isPermissionPermanentlyDenied = false
//        if (!uiHelper!!.isLocationProviderEnabled()) updateLocation()
////        if (!uiHelper!!.isLocationProviderEnabled()) subscribeLocationObserver()
//        else gpsSetting?.openGpsSettingDialog()
//    }

    private fun locationPermission() {
        if (ContextCompat.checkSelfPermission(
                this@LoginActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@LoginActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    this@LoginActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    this@LoginActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }
    }

    private fun registerLocation(id: Int, lat: Double, longi: Double) {

        compositeDisposable.add(myAPI.registerUserLocation(id, lat, longi)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                if (message.contains("")) {
                    val intent = Intent(this, MapBoxActivity::class.java)
                    //val intent = Intent(this, LawyerListActivity::class.java)
                    startActivity(intent)
                } else
                    Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            })


    }

//    private fun updateLocation() {
//
////        lat = locationTrack.getLatitude()
////        longi = locationTrack.getLongitude()
//
//        locationViewModel.getLocationData().observe(this, Observer {
//            latlng = LatLng(it.latitude, it.longitude)
//
//            lat = it.latitude
//            longi = it.longitude
//
////            Toast.makeText(this, "Latitude " + lat + "\nLongitude " + longi, Toast.LENGTH_LONG).show()
//
//        })
//
//        Toast.makeText(this, "Latitude " + lat + "\nLongitude " + longi, Toast.LENGTH_LONG).show()
//
//    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
////        callbackManager.onActivityResult(requestCode, resultCode, data)
//        super.onActivityResult(requestCode, resultCode, data)
//
//        when (requestCode) {
//            Constants.GPS_REQUEST_LOCATION ->
//                when (resultCode) {
//                    RESULT_OK -> updateLocation()
////                    RESULT_OK -> subscribeLocationObserver()
//
//                    RESULT_CANCELED -> {
//                        uiHelper?.showPositiveDialogWithListener(
//                            this,
//                            resources.getString(R.string.need_location),
//                            resources.getString(R.string.location_content),
//                            object : GpsEnableListener {
//                                override fun onPositive() {
//                                    enableGps()
//                                }
//                            }, resources.getString(R.string.turn_on), false
//                        )
//                    }
//                }
//        }
//    }

    private fun login(email: String, password: String) {

        compositeDisposable.add(myAPI.loginUser(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                if (message.contains("encrypted_password")) {

                    registerLocation(id, latitude, longitude)


//                    val intent = Intent(this, MenuActivity::class.java)
                    //val intent = Intent(this, LawyerListActivity::class.java)

                    if (message.contains("\"is_lawyer\":1")) {
                        Toast.makeText(
                            this@LoginActivity,
                            "Ingresando como abogado/a...",
                            Toast.LENGTH_LONG
                        ).show()
                        intent.putExtra("userType", "lawyer")
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            "Ingresando como cliente...",
                            Toast.LENGTH_LONG
                        ).show()
                        intent.putExtra("userType", "client")
                    }

                    startActivity(intent)
                } else
                    Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
            })
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }


    //    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        val isPermissionGranted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
//
//        if(!isPermissionGranted)
//        {
//            toast("Location Permission is not Granted.")
//            permissionHelper?.openSettingsDialog()
//        }
//        else {
//            if (locationTrack.canGetLocation) {
//                updateLocation()
//            }
//        }
//    }


//    override fun onRequestPermissionsResult(
//        requestCode: Int, permissions: Array<String>,
//        grantResults: IntArray
//
//
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode) {
//            1 -> {
//                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                ) {
//                    if ((ContextCompat.checkSelfPermission(
//                            this@LoginActivity,
//                            Manifest.permission.ACCESS_FINE_LOCATION
//                        ) ==
//                                PackageManager.PERMISSION_GRANTED)
//                    ) {
//                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
//                    }
//                } else {
//                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
//                }
//                return
//            }
//        }
//    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQ_CODE -> {
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {

                    // permission granted
                } else {
                    // permission denied
                    Toast.makeText(
                        this, "You need to grant permission to access location",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

//    override fun onPermissionResponse(isPermissionGranted: Boolean) {
//        if (!isPermissionGranted) isPermissionPermanentlyDenied = true
//        else enableGps()
//    }


}