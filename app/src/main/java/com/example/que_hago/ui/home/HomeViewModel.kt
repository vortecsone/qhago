package com.example.que_hago.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel()
{
    private val _text = MutableLiveData<String>().apply {
        value = "BIENVENIDO/A A QHAGO APP"
    }
    val text: LiveData<String> = _text
}