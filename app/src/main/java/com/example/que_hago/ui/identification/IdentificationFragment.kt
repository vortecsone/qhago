package com.example.que_hago.ui.identification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.que_hago.R

class IdentificationFragment : Fragment()
{
    private lateinit var identificationViewModel: IdentificationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        identificationViewModel =
            ViewModelProviders.of(this).get(IdentificationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_menu_identificacion, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
        identificationViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

}