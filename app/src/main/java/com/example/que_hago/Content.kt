package com.example.que_hago

data class Content(val title: String, val description: String, val image: Int)
