package com.example.que_hago

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.que_hago.localDB.SharedPreference
import com.example.que_hago.retrofit.INodeJS
import com.example.que_hago.retrofit.RetrofitClient
import com.example.que_hago.ui.login.LoginActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user_register_content_scrolling.*
import retrofit2.Retrofit

class  UserRegisterActivity : AppCompatActivity()
{
    lateinit var myAPI: INodeJS
    var compositeDisposable = CompositeDisposable()

    var editRut: EditText? = null
    var editName: EditText? = null
    var editLastname: EditText? = null
    var editEmail: EditText? = null
    var editPassword: EditText? = null
    var registerButton: Button? = null
    lateinit var sharedPreference:SharedPreference
    var id: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_register)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

         sharedPreference=SharedPreference(this)


        initUIControls()
        initDBApi()

        btnRegister.setOnClickListener {
            register(editRut!!.text.toString(), editEmail!!.text.toString(), editName!!.text.toString(), editLastname!!.text.toString(), editPassword!!.text.toString())
        }

        btnSkipRegister.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initUIControls()
    {
        editRut = findViewById(R.id.etRut)
        editName = findViewById(R.id.etName)
        editLastname = findViewById(R.id.etLastname)
        editEmail = findViewById(R.id.etEmail)
        editPassword = findViewById(R.id.etPassword)
        registerButton = findViewById(R.id.btnRegister)
    }

    private fun initDBApi()
    {
        val retrofit : Retrofit = RetrofitClient.instance
        myAPI = retrofit.create(INodeJS::class.java)
    }

    private fun register(rut: String, email: String, name: String, lastname: String, password: String)
    {
        compositeDisposable.add(myAPI.registerUser(rut,email,name,lastname,password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message ->
                val pattern = "[0-9]+".toRegex()
                val mFormatted = message.replace("\"", "");
                if (pattern.containsMatchIn(mFormatted))
                {
                    id = mFormatted.toInt()
                    sharedPreference.save("userID",id!!)
                    Toast.makeText(this@UserRegisterActivity, "REGISTRO CORRECTO", Toast.LENGTH_LONG).show()
                    val intent = Intent(this, UserRegisterValidation1Activity::class.java)
                    intent.putExtra("userID", id!!)
                    intent.putExtra("userRut", editRut!!.text.toString())
                    intent.putExtra("userName", editName!!.text.toString())
                    intent.putExtra("userLastname", editLastname!!.text.toString())
                    startActivity(intent)
                }
                else
                    Toast.makeText(this@UserRegisterActivity,message, Toast.LENGTH_LONG).show()
            })
    }

    override fun onStop()
    {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy()
    {
        compositeDisposable.clear()
        super.onDestroy()
    }




}