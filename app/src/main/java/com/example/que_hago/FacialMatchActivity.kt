package com.example.que_hago

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.appbar.CollapsingToolbarLayout
import kotlinx.android.synthetic.main.activity_facial_match_content_scrolling.*
import org.apache.commons.io.IOUtils


class FacialMatchActivity : AppCompatActivity()
{

    private val get_response_text: TextView? = null
    private var post_response_text: TextView? = null
    private var imageView: ImageView? = null
    var photoFile: File? = null
    var currentPhotoPath: String? = null
    var bitmapImage: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facial_match)
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        //Button get_request_button=findViewById(R.id.get_data);
        val post_request_button = findViewById<Button>(R.id.post_data)
        //val redirect_register = findViewById<Button>(R.id.redirect_register)

        //get_response_text=findViewById(R.id.get_respone_data);
        post_response_text = findViewById(R.id.post_respone_data)
        post_response_text!!.text = "CAPTURE UNA IMAGEN DE SU ROSTRO PARA INICIAL ANALISIS"
        imageView = findViewById(R.id.imageView)

        val filename = intent.getStringExtra("encodedBitmap")
        try
        {
            val `is` = openFileInput(filename)
            bitmapImage = BitmapFactory.decodeStream(`is`)
            setImageIntoView(bitmapImage!!)
            bitmapToBase64(bitmapImage!!)
            postRequest()
            `is`.close()
        }
        catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        btnSkipFace.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        post_request_button.setOnClickListener {
            val homepage = Intent(this@FacialMatchActivity, CameraActivity::class.java)
            startActivity(homepage)
            /*try
            {
                dispatchTakePictureIntent()
                Thread.sleep(10000)
                galleryAddPic()
                bitmapToBase64(setPic())
                postRequest()
            }
            catch (e: Exception) {
                e.printStackTrace()
            }*/
        }
        val x = intent.extras
        x?.getString(MediaStore.EXTRA_OUTPUT)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Throws(Exception::class)
    private fun postRequest()
    {
        val requestQueue = Volley.newRequestQueue(this)
        //val url = "http://8a2a0877d2e9.sn.mynetname.net:8081/iklab/ikface/api/match"
        val url = "http://50.192.137.252:8080/iklab/ikface/api/match?appKey=JDJiJDEwJGhwZC5ma3k5bnZFT3NIY0ZIUVJubC5nQlMvSUMyV0RBcTREYTRsV0lkOEpULzFMa0JIVVlPOmFkbWluOklLTEFCMDA0"
        //val atoken = IOUtils.toString(this.resources.openRawResource(R.raw.token), StandardCharsets.UTF_8)
        val imageb64 = readFromFile(this)
        val json = "{\"imageB64\":\"$imageb64\"}"
        val jsonBody = JSONObject(json)

        post_response_text!!.text = "REALIZANDO ANALISIS FACIAL..."
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(url, jsonBody, Response.Listener { response ->
            try
            {
                val jsonArray = response.getJSONArray("data")
                //var firstname = ""
                //var lastname = ""
                var fullname = ""
                var reliability = 0.0
                if (jsonArray != null && jsonArray.length() > 0)
                {
                    for (i in 0 until jsonArray.length())
                    {
                        val dataObject = jsonArray.getJSONObject(i)
                        //firstname = dataObject.getString("firstname")
                        //lastname = dataObject.getString("lastname")
                        fullname = dataObject.getString("fullName")
                        reliability = dataObject.getDouble("reliability")
                    }
                }
                //post_response_text!!.text = "MATCH : $firstname $lastname\n"
                post_response_text!!.text = "MATCH : $fullname\n"
                post_response_text!!.append("% DE COINCIDENCIA : $reliability")
                /*
                Thread.sleep(5000)
                val intent = Intent(this, MenuActivity::class.java)
                intent.putExtra("name", firstname)
                intent.putExtra("lastname", lastname)
                startActivity(intent)
                */
            }
            catch (e: JSONException) {
                post_response_text!!.text = "NO SE DETECTARON ROSTROS, INTENTE OTRA VEZ"
                e.printStackTrace()
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
        },
            Response.ErrorListener { post_response_text!!.text = "PROBLEMAS DE CONEXION, INTENTE OTRA VEZ" })
        {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>
            {
                val params: MutableMap<String, String> = HashMap()
                params["Content-Type"] = "application/json"
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    private fun dispatchTakePictureIntent()
    {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(packageManager) != null)
        {
            // Create the File where the photo should go
            //var photoFile: File? = null
            try
            {
                photoFile = createImageFile()
            }
            catch (ex: IOException)
            {
                post_response_text!!.text = ex.message
            }
            // Continue only if the File was successfully created
            if (photoFile != null)
            {
                val photoURI = FileProvider.getUriForFile(this, "com.example.android.gnprovider",
                    photoFile!!
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File
    {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.getExternalStorageDirectory().absolutePath + "/Pictures/")
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    private fun galleryAddPic()
    {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(currentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)
    }

    private fun setPic(): Bitmap
    {
        // Get the dimensions of the View
        val targetW = imageView!!.width
        val targetH = imageView!!.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH))

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true
        val bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        imageView!!.setImageBitmap(bitmap)
        return bitmap
    }

    private fun bitmapToBase64(bitmap: Bitmap): String
    {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP)
        writeToFile(encoded, this)
        return encoded
    }

    private fun writeToFile(data: String, context: Context)
    {
        try
        {
            val outputStreamWriter = OutputStreamWriter(context.openFileOutput("test.txt", MODE_PRIVATE))
            outputStreamWriter.write(data)
            outputStreamWriter.close()
        }
        catch (e: IOException) {
            Log.e("Exception", "File write failed: $e")
        }
    }

    private fun readFromFile(context: Context): String
    {
        var ret = ""
        try
        {
            val inputStream: InputStream? = context.openFileInput("test.txt")
            if (inputStream != null)
            {
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                var receiveString: String? = ""
                val stringBuilder = StringBuilder()
                while (bufferedReader.readLine().also { receiveString = it } != null) {
                    stringBuilder.append(receiveString)
                }
                inputStream.close()
                ret = stringBuilder.toString()
            }
        } catch (e: FileNotFoundException) {
            Log.e("login activity", "File not found: $e")
        } catch (e: IOException) {
            Log.e("login activity", "Can not read file: $e")
        }
        return ret
    }

    companion object
    {
        const val REQUEST_IMAGE_CAPTURE = 1
        @Throws(Exception::class)
        fun convertStreamToString(`is`: InputStream?): String
        {
            val reader = BufferedReader(InputStreamReader(`is`))
            val sb = StringBuilder()
            var line: String? = null
            while (reader.readLine().also { line = it } != null)
            {
                sb.append(line).append("\n")
            }
            reader.close()
            return sb.toString()
        }
        @Throws(Exception::class)
        fun getStringFromFile(filePath: String?): String
        {
            val fl = File(filePath)
            val fin = FileInputStream(fl)
            val ret = convertStreamToString(fin)
            //close all streams.
            fin.close()
            return ret
        }
    }

    private fun setImageIntoView(bm: Bitmap?)
    {
        imageView!!.setImageBitmap(bm)

    }


}